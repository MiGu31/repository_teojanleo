package com.servicio.restful;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import com.dao.comunes.daoLinea;
import com.dao.comunes.daoReclamo;
import com.entidades.comunes.entLinea;
import com.servicio.excepcion.customException;

@Controller
public class LineaController {
	
	@RequestMapping(value = "Linea/ListarLineas", method = RequestMethod.GET
		, produces="application/json")
		public @ResponseBody ArrayList<entLinea> ListarLineas() {
			ArrayList<entLinea> listaLineas = null;
			try {
				listaLineas = daoLinea.Instancia().ListarLineas();			
			}
			catch (Exception e) {
			e.printStackTrace();	
			}		
		return listaLineas;
		}
		
		@RequestMapping(value = "/Lineas/InsertarLineas", method = RequestMethod.GET, 
				produces ="application/json")
		public @ResponseBody Boolean InsertarLinea(String nombre,String imagen) {
			Boolean inserto = false;
			try {
				entLinea l = new entLinea();
					l.setNombre(nombre);
					l.setImagen(imagen);
				inserto = daoLinea.Instancia().InsertarLineass(l);
				} catch (Exception e) {
				e.printStackTrace();
				}
			return inserto;
		}
		
		@RequestMapping(value = "/Lineas/ModificarLineas", method = RequestMethod.GET, 
				produces ="application/json")
		public @ResponseBody Boolean p_updateLineas(int idLinea,String nombre,String imagen){
			Boolean edito = false;
			try {
				entLinea l = new entLinea();
				l.setIdLinea(idLinea);
				l.setNombre(nombre);
				l.setImagen(imagen);
				edito = daoLinea.Instancia().p_updateLineas(l);
				} catch (Exception e) {
				e.printStackTrace();
				}
			return edito;
		}
		
		@RequestMapping(value = "/Lineas/EliminarLineasPro", method = RequestMethod.GET
				, produces="application/json")
				public @ResponseBody Boolean p_deleteLinea(int idLinea){
					Boolean p = false;
					try {
						p = daoLinea.Instancia().p_deleteLineas(idLinea);			
					}
					catch (Exception e) {
					e.printStackTrace();	
					}		
				return p;
				}
		

}
