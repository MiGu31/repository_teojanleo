package com.servicio.restful;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.comunes.daoPersona;
import com.dao.comunes.daoReclamo;
import com.dao.comunes.daoUsuario;
import com.entidades.comunes.entPersona;
import com.entidades.comunes.entReclamo;


@Controller
public class ReclamoController {
	
	@RequestMapping(value = "Reclamo/ListarReclamos", method = RequestMethod.GET
	, produces="application/json")
	public @ResponseBody ArrayList<entReclamo> ListarReclamos() {
		ArrayList<entReclamo> listaReclamos = null;
		try {
			listaReclamos = daoReclamo.Instancia().ListarReclamos();	
		}
		catch (Exception e) {
		e.printStackTrace();	
		}		
	return listaReclamos;
	}
	
	@RequestMapping(value = "Reclamo/InsertarReclamo", method = RequestMethod.POST,  produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> InsertarReclamo(@RequestBody entReclamo r)
	{
		Boolean inserto = false;
		try {			
			inserto = daoReclamo.Instancia().InsertarReclamo(r);
			 return new ResponseEntity(HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			 return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}	   
	}
	
	@RequestMapping(value = "/Reclamo/CambiarEstado", method = RequestMethod.GET)
	public @ResponseBody Boolean CambiarEstado(int idReclamo) {
		Boolean cambio=false;
		try {
			cambio = daoReclamo.Instancia().CambiarEstado(idReclamo);			
		}
		catch (Exception e) {
		e.printStackTrace();	
		}		
	return cambio;
	}

}
