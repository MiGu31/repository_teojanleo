package com.servicio.restful;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.comunes.daoPromocion;
import com.dao.comunes.daoReclamo;
import com.dao.comunes.daoUsuario;
import com.entidades.comunes.entPromocion;
import com.entidades.comunes.entReclamo;
import com.entidades.comunes.entUsuario;
import com.servicio.email.correoElectronico;

@Controller
public class PromocionController {
	
	@RequestMapping(value = "Promocion/ListarPromociones", method = RequestMethod.GET
	, produces="application/json")
	public @ResponseBody ArrayList<entPromocion> ListarPromociones() {
		ArrayList<entPromocion> listaPromociones = null;
		try {
			listaPromociones = daoPromocion.Instancia().ListarPromociones();	
		}
		catch (Exception e) {
		e.printStackTrace();	
		}		
	return listaPromociones;
	}
	
	@RequestMapping(value = "/Promocion/Eliminar", method = RequestMethod.GET)
	public @ResponseBody Boolean EliminarPromocion(int idPromocion) {
		Boolean elimino=false;
		try {
			elimino =  daoPromocion.Instancia().EliminarPromocion(idPromocion);			
		}
		catch (Exception e) {
		e.printStackTrace();	
		}		
	return elimino;
	}
	
	@RequestMapping(value = "Promocion/InsertarPromocion", method = RequestMethod.POST,  produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> InsertarPromocion(@RequestBody entPromocion p)
	{
		Boolean inserto = false;
		try {			
			inserto = daoPromocion.Instancia().InsertarPromocion(p);
			ArrayList<entUsuario> listaUsuarios = daoUsuario.Instancia().ListarUsuarios();	
			correoElectronico.Instancia().avisoPromocion(p, listaUsuarios);		

			 return new ResponseEntity(HttpStatus.CREATED);
		} catch (Exception e) {
			//e.getMessage()
			 e.printStackTrace();
			 return new ResponseEntity(HttpStatus.BAD_REQUEST);	
			 }	   
	}
	
	@RequestMapping(value = "Promocion/ObtenerPromocion", method = RequestMethod.GET
	, produces="application/json")
	public @ResponseBody entPromocion ObtenerPromocion() {
		entPromocion objPromocion = null;
		try {
			objPromocion = daoPromocion.Instancia().ObtenerPromocion();	
		}
		catch (Exception e) {
		e.printStackTrace();	
		}		
	return objPromocion;
	}
	

}
