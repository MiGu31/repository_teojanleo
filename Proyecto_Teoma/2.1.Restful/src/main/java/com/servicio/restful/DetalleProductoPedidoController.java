package com.servicio.restful;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.comunes.daoDetalleProductoPedido;
import com.entidades.comunes.entDetalleProductoPedido;

@Controller
public class DetalleProductoPedidoController {
	
	@RequestMapping(value = "/DetalleProductoPedidoController/Listar", method = RequestMethod.GET
			, produces="application/json")
		public @ResponseBody ArrayList<entDetalleProductoPedido> ListarDetalleProductoPedido() {
			ArrayList<entDetalleProductoPedido> dpp = null;
			try {
				dpp = daoDetalleProductoPedido.Instancia().ListarDetalleProductoPedidos();
			}
			catch (Exception e) {
			e.printStackTrace();	
			}		
		return dpp;
	}

}
