package com.servicio.restful;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.comunes.daoDetalleProductoPedido;
import com.dao.comunes.daoDetalleProveedorProducto;
import com.dao.comunes.daoProveedor;
import com.dao.comunes.daoReclamo;
import com.entidades.comunes.entDetalleProductoPedido;
import com.entidades.comunes.entDetalleProveedorProducto;
import com.entidades.comunes.entProducto;
import com.entidades.comunes.entProveedor;

@Controller
public class ProveedorController {
	
	@RequestMapping(value = "Proveedor/ListarProveedores", method = RequestMethod.GET
	, produces="application/json")
	public @ResponseBody ArrayList<entProveedor> ListarProveedoresGET() {
		ArrayList<entProveedor> listaProveedores = null;
		try {
			listaProveedores = daoProveedor.Instancia().ListarProveedores();			
		}
		catch (Exception e) {
		e.printStackTrace();	
		}		
	return listaProveedores;
	}
	
	@RequestMapping(value = "Proveedor/InsertarProveedor", method = RequestMethod.POST,  produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> InsertarProveedorPOST(@RequestBody entProveedor objProveedor)
	{
		Boolean inserto = false;
		try {			
			inserto = daoProveedor.Instancia().InsertarProveedor(objProveedor);			
			
			 return new ResponseEntity(HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			 return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}	   
	}
	
	@RequestMapping(value = "Proveedor/ListarProductos", method = RequestMethod.GET
	, produces="application/json")
	public @ResponseBody ArrayList<entProducto> ListarProductosGET(int idProveedor) {
		ArrayList<entProducto> listaProductos = null;
		try {
			listaProductos = daoProveedor.Instancia().ListarProductosPorProveedor(idProveedor);			
		}
		catch (Exception e) {
		e.printStackTrace();	
		}		
	return listaProductos;
	}

	
}
