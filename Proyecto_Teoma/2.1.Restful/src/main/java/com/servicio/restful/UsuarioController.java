package com.servicio.restful;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.comunes.daoPersona;
import com.dao.comunes.daoReclamo;
import com.dao.comunes.daoUsuario;
import com.entidades.comunes.entPersona;
import com.entidades.comunes.entReclamo;
import com.entidades.comunes.entUsuario;

@Controller
public class UsuarioController {
	
	@RequestMapping(value = "Usuario/VerificarAcceso", method = RequestMethod.GET
			  , produces="application/json")
	public @ResponseBody entUsuario VerificarAcceso(String Username,String Password) {
	entUsuario u = null;
		try {
			u = daoUsuario.Instancia().VerificarAcceso(Username, Password);			
		}
		catch (Exception e) {
		e.printStackTrace();	
		}	
			return u;
	}	
	
	@RequestMapping(value = "/Usuario/CambiarEstado", method = RequestMethod.GET)
	public @ResponseBody Boolean CambiarEstado(int idUsuario) {
		Boolean cambio=false;
		try {
			cambio = daoUsuario.Instancia().CambiarEstado(idUsuario);			
		}
		catch (Exception e) {
		e.printStackTrace();	
		}		
	return cambio;
	}

}
