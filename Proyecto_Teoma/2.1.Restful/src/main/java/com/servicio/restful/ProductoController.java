package com.servicio.restful;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.dao.comunes.daoLinea;
import com.dao.comunes.daoProducto;
import com.dao.comunes.daoReclamo;
import com.entidades.comunes.entLinea;
import com.entidades.comunes.entProducto;
import com.entidades.comunes.entReclamo;

@Controller
public class ProductoController {
	
	@RequestMapping(value = "Producto/ListarProductos", method = RequestMethod.GET
	, produces="application/json")
	public @ResponseBody ArrayList<entProducto> ListarProductos() {
		ArrayList<entProducto> listaProductos = null;
		try {
			listaProductos = daoProducto.Instancia().ListarProductos();			
		}
		catch (Exception e) {
		e.printStackTrace();	
		}		
	return listaProductos;
	}
	
	@RequestMapping(value = "/Producto/InsertarProducto", method = RequestMethod.GET, 
			produces ="application/json")
	public @ResponseBody Boolean InsertarProductoGET(int idLinea, String nombre, Float precio,
			String descripcion,String imagen) {
		Boolean inserto = false;
		try {
			entProducto p = new entProducto();
			entLinea l = new entLinea();
			l.setIdLinea(idLinea);
			p.setObjLinea(l);
			p.setNombre(nombre);
			p.setPrecio(precio);
			p.setDescripcion(descripcion);
			p.setImagen(imagen);
			inserto = daoProducto.Instancia().InsertarProducto(p);
			} catch (Exception e) {
			e.printStackTrace();
			}
		return inserto;
	}
	
	@RequestMapping(value = "Producto/InsertarProducto", method = RequestMethod.POST,  produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> InsertarProductoPOST(@RequestBody entProducto p)
	{
		Boolean inserto = false;
		try {			
			inserto = daoProducto.Instancia().InsertarProducto(p);	
			 return new ResponseEntity(HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			 return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}	   
	}
	
	@RequestMapping(value = "Producto/EditarProducto", method = RequestMethod.POST,  produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> EditarProductoPOST(@RequestBody entProducto p)
	{
		Boolean inserto = false;
		try {			
			inserto = daoProducto.Instancia().EditarProducto(p);	
			 return new ResponseEntity(HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			 return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}	   
	}
	
	@RequestMapping(value = "/Producto/Eliminar", method = RequestMethod.GET)
	public @ResponseBody Boolean EliminarProducto(int idProducto) {
		Boolean elimino=false;
		try {
			elimino = daoProducto.Instancia().EliminarProducto(idProducto);			
		}
		catch (Exception e) {
		e.printStackTrace();	
		}		
	return elimino;
	}
	
	@RequestMapping(value = "/Producto/ModificarProducto", method = RequestMethod.GET, 
			produces ="application/json")
	public @ResponseBody Boolean p_UpdateProductos(int idProducto,Float precio,String imagen){
		Boolean edito = false;
		try {
			entProducto p = new entProducto();
			p.setIdProducto(idProducto);
			p.setPrecio(precio);
			p.setImagen(imagen);
			edito = daoProducto.Instancia().p_UpdateProductos(p);
			} catch (Exception e) {
			e.printStackTrace();
			}
		return edito;
	}
	
	@RequestMapping(value = "Producto/EliminarProducto", method = RequestMethod.GET
			, produces="application/json")
			public @ResponseBody Boolean p_deleteProductos(int idProducto){
				Boolean p = null;
				try {
					p = daoProducto.Instancia().p_deleteProductos(idProducto);			
				}
				catch (Exception e) {
				e.printStackTrace();	
				}		
			return p;
			}

}
