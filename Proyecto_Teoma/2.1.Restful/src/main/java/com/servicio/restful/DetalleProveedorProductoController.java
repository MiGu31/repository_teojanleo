package com.servicio.restful;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.comunes.daoDetalleProductoPedido;
import com.dao.comunes.daoDetalleProveedorProducto;
import com.dao.comunes.daoProducto;
import com.entidades.comunes.entDetalleProductoCompra;
import com.entidades.comunes.entDetalleProductoPedido;
import com.entidades.comunes.entDetalleProveedorProducto;
import com.entidades.comunes.entProducto;
import com.entidades.comunes.entProveedor;

@Controller
public class DetalleProveedorProductoController {
	
	@RequestMapping(value = "DetalleProveedorProducto/Insertar", method = RequestMethod.POST,  produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> InsertarDetalleProductoPOST(@RequestBody entProveedor objProveedor)
	{
		Boolean inserto = false;
		try {			
			
			ArrayList<entDetalleProveedorProducto> objEntDetalle = new ArrayList<entDetalleProveedorProducto>();
			
			for(entProducto objProducto : objProveedor.getListaProductos()) {
				
				inserto = daoDetalleProveedorProducto.Instancia().InsertarDetalleProveedorProducto(objProveedor, objProducto);		
									
			}	
			
			 return new ResponseEntity(HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			 return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}	   
	}

}
