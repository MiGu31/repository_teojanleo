package com.servicio.restful;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.comunes.daoCompra;
import com.dao.comunes.daoDetalleProductoCompra;
import com.dao.comunes.daoPedido;
import com.entidades.comunes.entCompra;
import com.entidades.comunes.entDetalleProductoCompra;
import com.entidades.comunes.entPedido;
import com.servicio.email.correoElectronico;
import com.servicio.email.correoElectronicoPedido;
import com.servicio.excepcion.customException;


@Controller
public class CompraController {
		
	
	public float determinarCostoTotal(entCompra objCompra) throws customException{
		float costoTotal = 0;
		
		
		for(entDetalleProductoCompra dc : objCompra.getListaDetalle()) {
			costoTotal += dc.getCantidad() * dc.getObjProducto().getPrecio();
		}	
		
		if(costoTotal>0)
		{	
			
			return costoTotal;
		}
		else
		{
			throw new customException("El valor del costo total es 0, corrige tus sentencias");
		}

	}
	
	public void insertarCompraEnDetalle(entCompra objCompra) {
		for(entDetalleProductoCompra dc : objCompra.getListaDetalle()) {
			dc.setObjCompra(objCompra);
		}		
	}
	
	
	@RequestMapping(value = "Compra/InsertarCompra", method = RequestMethod.POST,  produces = "application/json")
	@ResponseBody
	public ResponseEntity<entCompra> InsertarCompraPost(@RequestBody entCompra c)
	{
		Boolean inserto = false;
		try {								
			
			c.setCostoTotal(determinarCostoTotal(c));			
			
			inserto = daoCompra.Instancia().InsertarCompra(c);

			insertarCompraEnDetalle(c);
			
			if(!inserto) {
				inserto = daoDetalleProductoCompra.Instancia().InsertarDetalleCompra(c.getListaDetalle());
			}
			
		
			 return new ResponseEntity(HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			 return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}	   
	}
	
	@RequestMapping(value = "Compra/ListarCompras", method = RequestMethod.GET
	, produces="application/json")
	private @ResponseBody ArrayList<entCompra> ListarCompras() {
		ArrayList<entCompra> listaCompras = null;
		try {
			listaCompras = daoCompra.Instancia().ListarCompras();		
		}
		catch (Exception e) {
		e.printStackTrace();	
		}		
	return listaCompras;
	}

}
