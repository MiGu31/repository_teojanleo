package com.servicio.restful;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.comunes.daoDetalleProductoPedido;
import com.dao.comunes.daoPedido;
import com.dao.comunes.daoPersona;
import com.dao.comunes.daoPromocion;
import com.entidades.comunes.entDetalleProductoPedido;
import com.entidades.comunes.entPedido;
import com.entidades.comunes.entPersona;
import com.entidades.comunes.entPromocion;
import com.servicio.email.correoElectronicoPedido;

@Controller
public class PedidoController {	
	
	public float recorredDetalleYCalcularCostoTotal(entPedido p) {
		
		float costoTotal = 0;

		for(entDetalleProductoPedido dp : p.getListaDetalle()) {
			costoTotal += dp.getCantidad() * dp.getObjProducto().getPrecio();
		}
		return costoTotal;
	}
	
	public float cambiarCostoTotal(float costoTotal, entPedido p) {
		
		p.setCostoTotal(costoTotal);
		return p.getCostoTotal();		
	}
	
	@RequestMapping(value = "Pedido/InsertarPedido", method = RequestMethod.POST,  produces = "application/json")
	@ResponseBody
	public ResponseEntity<entPedido> InsertarPedido(@RequestBody entPedido p) throws Exception
	{
		final float calcularCostoTotal = recorredDetalleYCalcularCostoTotal(p);
		final entPedido objPedido = p;
		entPromocion objPromocion = daoPromocion.Instancia().ObtenerPromocion();
		objPedido.setObjPromocion(objPromocion);
		
		Boolean inserto = false;
		try {				
					
			p.setCostoTotal(cambiarCostoTotal(calcularCostoTotal,objPedido));
						
			inserto = daoPedido.Instancia().InsertarPedido(p);
			
			for(entDetalleProductoPedido dp : p.getListaDetalle()) {
				dp.setObjPedido(p);
			}
			
			if(!inserto) {
				inserto = daoDetalleProductoPedido.Instancia().InsertarDetallePedido(p.getListaDetalle());
			}
			
			correoElectronicoPedido.Instancia().avisoPedido(p);		
			
			 return new ResponseEntity(HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			 return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}	   
	}
	
	
	@RequestMapping(value = "Pedido/ListarPedidos", method = RequestMethod.GET
	, produces="application/json")
	public @ResponseBody ArrayList<entPedido> ListarPedidos(int idUsuario) {
		ArrayList<entPedido> listaPedidos = null;
		try {
			listaPedidos = daoPedido.Instancia().ListarPedidos(idUsuario);		
		}
		catch (Exception e) {
		e.printStackTrace();	
		}		
	return listaPedidos;
	}
	
	@RequestMapping(value = "Pedido/ListarPedidosGenerales", method = RequestMethod.GET
	, produces="application/json")
	public @ResponseBody ArrayList<entPedido> ListarPedidosGenerales() {
		ArrayList<entPedido> listaPedidosGenerales = null;
		try {
			listaPedidosGenerales = daoPedido.Instancia().ListarPedidosGenerales();		
		}
		catch (Exception e) {
		e.printStackTrace();	
		}		
	return listaPedidosGenerales;
	}

}
