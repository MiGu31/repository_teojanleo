package com.servicio.restful;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.comunes.daoDetalleProductoCompra;
import com.entidades.comunes.entDetalleProductoCompra;

@Controller
public class DetalleProductoCompraController {
	
	@RequestMapping(value = "/DetalleProductoCompra/Listar", method = RequestMethod.GET
			, produces="application/json")
		public @ResponseBody ArrayList<entDetalleProductoCompra> ListarDetalleProductoCompra() {
			ArrayList<entDetalleProductoCompra> dpc = null;
			try {
				dpc = daoDetalleProductoCompra.Instancia().ListarDetalleProductoCompras();
			}
			catch (Exception e) {
			e.printStackTrace();	
			}		
		return dpc;
	}

}
