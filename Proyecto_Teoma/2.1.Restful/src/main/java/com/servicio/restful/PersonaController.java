package com.servicio.restful;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.comunes.daoPersona;
import com.entidades.comunes.entPersona;
import com.entidades.comunes.entUsuario;

@Controller
public class PersonaController {
	
	@RequestMapping(value = "/Persona/ListarPersonas", method = RequestMethod.GET
			, produces="application/json")
		public @ResponseBody ArrayList<entPersona> ListarPersona() {
			ArrayList<entPersona> p = null;
			try {
				p = daoPersona.Instancia().ListarPersonas();			
			}
			catch (Exception e) {
			e.printStackTrace();	
			}		
		return p;
	}
		
	@RequestMapping(value = "/Persona/ListarPersonaN", method = RequestMethod.GET
			, produces="application/json")
		public @ResponseBody ArrayList<entPersona> ListarPersonaN() {
			ArrayList<entPersona> p = null;
			try {
				p = daoPersona.Instancia().p_ListarPersonas();			
			}
			catch (Exception e) {
			e.printStackTrace();	
			}		
		return p;
	}
		
	@RequestMapping(value = "/Persona/InsertarPersona", method = RequestMethod.GET, 
			produces ="application/json")
	public @ResponseBody Boolean InsertarPersona(String dni, String nombres, String apellidos, 
			 String telefonoCelular,String telefonoFijo,String direccion, int idUsuario) {
		Boolean inserto = false;
		try {
			entPersona p = new entPersona();
			entUsuario u = new entUsuario();
			p.setDni(dni);
			p.setNombres(nombres);
			p.setApellidos(apellidos);
			p.setTelefonoCelular(telefonoCelular);
			p.setTelefonoFijo(telefonoFijo);
			p.setDireccion(direccion);
			u.setIdUsuario(idUsuario);
			p.setObjUsuario(u);
			inserto = daoPersona.Instancia().InsertarPersonass(p);
			} catch (Exception e) {
			e.printStackTrace();
			}
		return inserto;
	}
	
	
	@RequestMapping(value = "/Persona/InsertarCliente", method = RequestMethod.POST,  produces = "application/json", consumes="application/json")
	@ResponseBody
	public ResponseEntity<String> InsertarUsuarioPost(@RequestBody entPersona p)
	{
		Boolean inserto = false;
		try {			
			inserto = daoPersona.Instancia().InsertarCliente(p);
			 return new ResponseEntity(HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			 return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}	   
	}
	
	@RequestMapping(value = "/Persona/EditarCliente", method = RequestMethod.POST,  produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> EditarUsuarioPOST(@RequestBody entPersona p)
	{
		Boolean inserto = false;
		try {			
			inserto = daoPersona.Instancia().EditarPersona(p);;
			 return new ResponseEntity(HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			 return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}	   
	}
	
	@RequestMapping(value = "/Persona/ListarClientes", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody ArrayList<entPersona> ListarClientes() {
		ArrayList<entPersona> listaClientes = null;
		try {
			listaClientes = daoPersona.Instancia().ListarClientes();	
		}
		catch (Exception e) {
		e.printStackTrace();	
		}		
	return listaClientes;
	}
	
	@RequestMapping(value = "/Persona/ModificarPersona", method = RequestMethod.GET, 
			produces ="application/json")
	public @ResponseBody Boolean p_UpdatePersonas(int idPersona,String dni,String nombres, String apellidos,
			String telefonoCelular,String telefonoFijo,String direccion){
		Boolean edito = false;
		try {
			entPersona p = new entPersona();
			p.setIdPersona(idPersona);
			p.setDni(dni);
			p.setNombres(nombres);
			p.setApellidos(apellidos);
			p.setTelefonoCelular(telefonoCelular);
			p.setTelefonoFijo(telefonoFijo);
			p.setDireccion(direccion);
			
			edito = daoPersona.Instancia().p_UpdatePersonas(p);
			} catch (Exception e) {
			e.printStackTrace();
			}
		return edito;
	}
	
	@RequestMapping(value = "/Persona/EliminarPersona", method = RequestMethod.GET
			, produces="application/json")
			public @ResponseBody Boolean p_deletePersona(int idPersona){
				Boolean p = null;
				try {
					p = daoPersona.Instancia().p_deletePersonas(idPersona);			
				}
				catch (Exception e) {
				e.printStackTrace();	
				}		
			return p;
			}

}
