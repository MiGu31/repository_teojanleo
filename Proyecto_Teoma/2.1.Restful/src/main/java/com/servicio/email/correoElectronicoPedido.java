package com.servicio.email;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.entidades.comunes.entDetalleProductoPedido;
import com.entidades.comunes.entPedido;


public class correoElectronicoPedido {
	
	public static correoElectronicoPedido _Instancia;
	private correoElectronicoPedido() {};
	public static correoElectronicoPedido Instancia() {
		if(_Instancia==null) {
			_Instancia = new correoElectronicoPedido();
		}
		return _Instancia;
	}
	
	public String crearMensaje(entPedido objPedido) {
		
		ArrayList<String> listaString = new ArrayList<String>();
		
		
		int year = Calendar.getInstance().get(Calendar.YEAR);
		String cadenita1 =
				"<table style=\"max-width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;\">\n" +
                "\n" +
                "\n" +
                "\t<tr>\n" +
                "\t\t<td style=\"padding: 0\">\n" +
                "\t\t\t<img style=\"padding: 0; display: block\" src=\"http://teomacorp.com/wp-content/uploads/2017/05/mundo.jpg\" width=\"100%\">\n" +
                "\t\t</td>\n" +
                "\t</tr>\n" +
                "\t\n" +
                "\t<tr>\n" +
                "\t\t<td style=\"background-color: #ecf0f1\">\n" +
                "\t\t\t<div style=\"color: #34495e; margin: 4% 10% 2%; text-align: justify;font-family: sans-serif\">\n" +
                "\t\t\t\t<h2 style=\"color: #e67e22; margin: 0 0 7px\">Gracias por comprar en TEOJANLEO</h2>\n" +
                "\t\t\t\t<p style=\"margin: 2px; font-size: 15px\">\n" +
                "\t\t\t\t\tA continuacion el detalle de su compra:"+ "\n" +
                "\t\t\t\t</p>\n" +
                "\t\t\t\t<br>\n" +
               
                "\n" +
                "\n" +"<table width='100%' border='1' align='center'>"
                + "<tr align='center'>"
                + "<td><b>Nombre del producto<b></td>"
                + "<td><b>Precio unitario<b></td>"
                + "<td><b>Cantidad<b></td>"
                + "<td><b>Importe<b></td>";                
             
		 for(entDetalleProductoPedido objDetallePro: objPedido.getListaDetalle()) {
			   cadenita1 = cadenita1+ "<tr align='center'>"+"<td>" + String.valueOf(objDetallePro.getObjProducto().getNombre()) + "</td>"
                       + "<td>" + String.valueOf(objDetallePro.getObjProducto().getPrecio()) + "</td>"
                       + "<td>" + String.valueOf(objDetallePro.getCantidad())+ "</td>"
                       + "<td>" + String.valueOf(objDetallePro.getCantidad()*objDetallePro.getObjProducto().getPrecio())+ "</tr>";
		 }
		 cadenita1 = cadenita1 + 
                "</table></br></br>\t\t\t\t<div style=\"width: 100%; text-align: left\">\n"+				
                "\t\t\t\t\t<h3>CaracterÝsticas del pedido:\n </h3>" +
                "\t\t\t\t<ul style=\"font-size: 15px;  margin: 10px 0\">\n" +
                "\t\t\t\t\t<li>Valor de la venta: "+String.valueOf(objPedido.getCostoTotal())+"</li>\n" +
                "\t\t\t\t\t<li>Descuento: "+String.valueOf(objPedido.getCostoTotal()*((objPedido.getObjPromocion().getValor()*0.01)))+"</li>\n" +
                "\t\t\t\t\t<li>Total a pagar: "+String.valueOf((objPedido.getCostoTotal())-(objPedido.getCostoTotal()*((objPedido.getObjPromocion().getValor()*0.01))))+"</li>\n" +
                "</ul>"+
                "</br>"+
                "</div>";
		
		return cadenita1;
	}
	
	public void avisoPedido(entPedido objPedido) throws Exception {
		
		final String username = "teojanleo@gmail.com";
        final String password = "qweret789456123";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.ssl.trust", "*");
        

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });


        try {

			
            Message message = new MimeMessage(session);            
            
            /*
            InternetAddress[] address = new InternetAddress[listaUsuarios.size()];
            for (int i = 0; i < listaUsuarios.size(); i++) {
            	address[i] = new InternetAddress(listaUsuarios.get(i).getEmail());;     	
            }*/
            //message.setRecipients(Message.RecipientType.TO, address);
            message.setFrom(new InternetAddress("no-reply@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, 
            		InternetAddress.parse(objPedido.getObjUsuario().getEmail()));
            message.setSubject("Detalle de tu ultima compra");
            message.setText(crearMensaje(objPedido));
            message.setContent(crearMensaje(objPedido).toString(),"text/html");
            //message.set
            Transport.send(message);    		
					
		 	    		    		
            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
			
	}


}
