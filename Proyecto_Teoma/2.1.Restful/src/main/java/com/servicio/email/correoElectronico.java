package com.servicio.email;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.entidades.comunes.entPromocion;
import com.entidades.comunes.entUsuario;

public class correoElectronico {
	
	public static correoElectronico _Instancia;
	private correoElectronico() {};
	public static correoElectronico Instancia() {
		if(_Instancia==null) {
			_Instancia = new correoElectronico();
		}
		return _Instancia;
	}
	
	public String crearMensaje(entPromocion objPromocion) {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		String cadenita =
				"<table style=\"max-width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;\">\n" +
                "\n" +
                "\n" +
                "\t<tr>\n" +
                "\t\t<td style=\"padding: 0\">\n" +
                "\t\t\t<img style=\"padding: 0; display: block\" src=\"http://teomacorp.com/wp-content/uploads/2017/05/mundo.jpg\" width=\"100%\">\n" +
                "\t\t</td>\n" +
                "\t</tr>\n" +
                "\t\n" +
                "\t<tr>\n" +
                "\t\t<td style=\"background-color: #ecf0f1\">\n" +
                "\t\t\t<div style=\"color: #34495e; margin: 4% 10% 2%; text-align: justify;font-family: sans-serif\">\n" +
                "\t\t\t\t<h2 style=\"color: #e67e22; margin: 0 0 7px\">Querido miembro de la familia TEOMA</h2>\n" +
                "\t\t\t\t<p style=\"margin: 2px; font-size: 15px\">\n" +
                "\t\t\t\t\tTe informamos que acaba de iniciar la promoción: "+objPromocion.getNombre()+"\n" +
                "\t\t\t\t</p>\n" +
                "\t\t\t\t<br>\n" +
                "\t\t\t\t<p style=\"margin: 2px; font-size: 15px\">\n" +
                "\t\t\t\t\tCaracterísticas de la promoción:\n" +
                "\t\t\t\t</p>\n" +
                "\t\t\t\t<ul style=\"font-size: 15px;  margin: 10px 0\">\n" +
                "\t\t\t\t\t<li>Fecha de vencimiento: "+objPromocion.getFechaCaducidad().toString()+"</li>\n" +
                "\t\t\t\t\t<li>Descuento: "+objPromocion.getValor()+" </li>\t\t\t\t\n" +
                "\t\t\t\t</br>\n" +
                "\n" +
                "\t\t\t\t<div style=\"width: 100%; text-align: center\">\n" +
                "\t\t\t\t\t<a style=\"text-decoration: none; border-radius: 5px; padding: 11px 23px; color: white; background-color: #3498db\" href=\"https://server-web-teoma.azurewebsites.net/Web/Login\">Iniciar sesión</a>\t\n" +
                "\t\t\t\t</div>\n" +
                "\t\t\t\t<p style=\"color: #b3b3b3; font-size: 12px; text-align: center;margin: 30px 0 0\">Teoma Trujillo - "+year+"</p>\n" +
                "\t\t\t</div>\n" +
                "\t\t</td>\n" +
                "\t</tr>\n" +
                "</table>";
		
		return cadenita;
	}
	
	public void avisoPromocion(entPromocion objPromocion, ArrayList<entUsuario> listaUsuarios) throws Exception {
							
		final String username = "teojanleo@gmail.com";
        final String password = "qweret789456123";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.ssl.trust", "*");
        

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });


        try {

			
            Message message = new MimeMessage(session);            
            
            InternetAddress[] address = new InternetAddress[listaUsuarios.size()];
            for (int i = 0; i < listaUsuarios.size(); i++) {
            	address[i] = new InternetAddress(listaUsuarios.get(i).getEmail());;     	
            }
            message.setRecipients(Message.RecipientType.TO, address);
            message.setFrom(new InternetAddress("no-reply@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,address);
            message.setSubject("Promoción Teoma : "+objPromocion.getNombre());
            message.setText(crearMensaje(objPromocion));
            message.setContent(crearMensaje(objPromocion).toString(),"text/html");
            //message.set
            Transport.send(message);    		
					
		 	    		    		

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
			
	}

}
