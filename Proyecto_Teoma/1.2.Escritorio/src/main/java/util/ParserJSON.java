package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ParserJSON {
	 public static JSONObject LeerJSONObjectdesdeURL(String url) throws IOException, JSONException{
	        InputStream in = new URL(url).openStream();
	        try{
	            BufferedReader rd = new BufferedReader(new InputStreamReader(in, Charset.forName("UTF-8")));
	            String jsonTexto = readAll(rd);
	            return new JSONObject(jsonTexto);
	        }finally {
	            in.close();
	        }
	    }

	    public static JSONArray LeerJSONArraydesdeURL(String url) throws IOException, JSONException{
	        InputStream in = new URL(url).openStream();
	        try{
	            BufferedReader rd = new BufferedReader(new InputStreamReader(in, Charset.forName("UTF-8")));
	            String jsonTexto = readAll(rd);
	            return new JSONArray(jsonTexto);
	        }finally {
	            in.close();
	        }
	    }
	    
	    public static String LeerStringdesdeURL(String url) throws Exception{
	        InputStream in = null;
	        URL Url = null;
	        HttpURLConnection conn = null;
	        try {
	            Url = new URL(url);
	            conn = (HttpURLConnection)Url.openConnection();
	            conn.setConnectTimeout(30000);
	            conn.setReadTimeout(30000);
	            in = conn.getInputStream();
	            //in = new URL(url).openStream();

	            BufferedReader rd = new BufferedReader(new InputStreamReader(in, Charset.forName("UTF-8")));
	            String jsonTexto = readAll(rd);
	            return jsonTexto;
	        } catch (Exception e) {
	            throw e;
	        }finally {
	            if (in != null) {
	                try {
	                    in.close();
	                } catch (IOException ignored) {
	                }
	            }
	            if (conn != null) {
	                conn.disconnect();
	            }
	        }
	    }

	    private static String readAll(Reader rd) throws IOException{
	        StringBuilder sb = new StringBuilder();
	        int cp;
	        while((cp = rd.read())!=-1){
	            sb.append((char) cp);
	        }
	        return sb.toString();
	    }

}
