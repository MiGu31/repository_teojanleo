package formularios;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import com.entidades.comunes.entLinea;
import com.entidades.comunes.entProducto;
import com.fasterxml.jackson.databind.util.Converter;

import ConsumoRest.restProducto;

public class frmGestionProducto {

	private JFrame frame;
	private JTextField txtLinea;
	private JTextField txtNombre;
	private JTextField txtPrecio;
	private JTextField txtDescripcion;
	private JTextField txtImagen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmGestionProducto window = new frmGestionProducto();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public frmGestionProducto() {
		initialize();
	}
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 371);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblLinea = new JLabel("Linea:");
		lblLinea.setBounds(24, 40, 46, 14);
		frame.getContentPane().add(lblLinea);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(24, 78, 46, 14);
		frame.getContentPane().add(lblNombre);
		
		JLabel lblPrecio = new JLabel("Precio:");
		lblPrecio.setBounds(24, 124, 46, 14);
		frame.getContentPane().add(lblPrecio);
		
		JLabel lblDescripcion = new JLabel("Descripcion:");
		lblDescripcion.setBounds(24, 167, 74, 14);
		frame.getContentPane().add(lblDescripcion);
		
		JLabel lblImagen = new JLabel("Imagen:");
		lblImagen.setBounds(24, 212, 46, 14);
		frame.getContentPane().add(lblImagen);
		
		txtLinea = new JTextField();
		txtLinea.setBounds(100, 37, 112, 20);
		frame.getContentPane().add(txtLinea);
		txtLinea.setColumns(10);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(100, 75, 112, 20);
		frame.getContentPane().add(txtNombre);
		txtNombre.setColumns(10);
		
		txtPrecio = new JTextField();
		txtPrecio.setBounds(100, 121, 112, 20);
		frame.getContentPane().add(txtPrecio);
		txtPrecio.setColumns(10);
		
		txtDescripcion = new JTextField();
		txtDescripcion.setBounds(100, 164, 112, 20);
		frame.getContentPane().add(txtDescripcion);
		txtDescripcion.setColumns(10);
		
		txtImagen = new JTextField();
		txtImagen.setBounds(100, 209, 112, 20);
		frame.getContentPane().add(txtImagen);
		txtImagen.setColumns(10);
		
		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				insertarProducto();
			}
		});
		btnRegistrar.setBounds(24, 264, 89, 23);
		frame.getContentPane().add(btnRegistrar);
		
		JButton btnCerrar = new JButton("Cerrar");
		btnCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnCerrar.setBounds(123, 264, 89, 23);
		frame.getContentPane().add(btnCerrar);
	}

	public void insertarProducto() {
		try {
			entProducto p = new entProducto();
			entLinea l = new entLinea();
			l.setIdLinea(Integer.parseInt(txtLinea.getText()));
			p.setObjLinea(l);
			p.setNombre(txtNombre.getText());
			p.setPrecio(Double.parseDouble(txtPrecio.getText()));
			p.setDescripcion(txtDescripcion.getText());
			p.setImagen(txtImagen.getText());
			boolean x = restProducto.Instancia().insertarProducto(p); 
			if(x) {
				JOptionPane.showMessageDialog(null,"Se inserto Satisfactoriamente el Producto","Registro Producto", JOptionPane.INFORMATION_MESSAGE);		
			}else {
				JOptionPane.showMessageDialog(null,"Error al inserto el Producto","Registro Producto", JOptionPane.INFORMATION_MESSAGE);		
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
	}
}
