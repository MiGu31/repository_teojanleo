package ConsumoRest;

import java.io.IOException;

import com.entidades.comunes.entProducto;

import util.ParserJSON;

public class restProducto {
	
	//Sinlegton
		public static restProducto _Instancia;
		public static restProducto Instancia() {
			if(_Instancia==null) {
				_Instancia = new restProducto();
			}
			return _Instancia;
		}
		//Sinlegton
		
		public boolean insertarProducto(entProducto p) throws Exception{
		 	String urlPost = "http://localhost:8081/restful/Producto/InsertarProducto?"+
		 	"idLinea="+p.getObjLinea().getIdLinea()+
		 	"&nombre="+p.getNombre()+
		 	"&precio="+p.getPrecio()+
		 	"&descripcion="+p.getDescripcion()+
		 	"&imagen="+p.getImagen();
	        String strDatos = "";
	        String error = "";	
	        boolean x =false; 
		 try {
             strDatos = ParserJSON.LeerStringdesdeURL(urlPost);
             if(strDatos.equals("true")){
            	 x= true;
             }
         } catch (IOException e) {
             e.printStackTrace();
             error = e.getMessage();
         } catch (Exception e) {
             e.printStackTrace();
             error = e.getMessage();
         }
		 return x;
	}
	 

}
