package com.dao.comunes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;

import com.entidades.comunes.entCompra;
import com.entidades.comunes.entPedido;
import com.entidades.comunes.entPromocion;
import com.entidades.comunes.entProveedor;
import com.entidades.comunes.entUsuario;

public class daoCompra {
	
	public static daoCompra _Instancia;
	private daoCompra() {};
	public static daoCompra Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoCompra();
		}
		return _Instancia;
	}	
	
	public boolean InsertarCompra(entCompra c) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		
		try {
			CallableStatement cst = cn.prepareCall("{call InsertarCompra(?,?,?,?)}");
			cst.setInt(1, c.getObjUsuario().getIdUsuario());
			cst.setFloat(2, c.getCostoTotal());
			cst.setInt(3, c.getObjProveedor().getIdProveedor());
			cst.registerOutParameter(4, Types.INTEGER);
			
			inserto = cst.execute();
			
			if(!inserto) {
				c.setIdCompra(cst.getInt("id"));
			}
			
		} catch (Exception e) {
			throw e;
		}finally {
			cn.close();
		}
		
		return inserto;				
	}
	
	public ArrayList<entCompra> ListarCompras() throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entCompra> listaCompras = new ArrayList<entCompra>();
		try {
			CallableStatement cst = cn.prepareCall("{call ListarCompras()}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				entCompra co = new entCompra();
					co.setIdCompra(rs.getInt("idCompra"));
					co.setFecha(rs.getDate("Fecha"));
					co.setCostoTotal(rs.getFloat("CostoTotal"));
					entProveedor p = new entProveedor();
						p.setNombre(rs.getString("Nombre"));;
					co.setObjProveedor(p);						
					entUsuario u = new entUsuario();
						u.setUsername(rs.getString("Username"));
					co.setObjUsuario(u);

					listaCompras.add(co);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaCompras;
	}

}
