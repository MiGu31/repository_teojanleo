package com.dao.comunes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.entidades.comunes.entCompra;
import com.entidades.comunes.entDetalleProductoCompra;
import com.entidades.comunes.entDetalleProductoPedido;
import com.entidades.comunes.entPedido;
import com.entidades.comunes.entProducto;

public class daoDetalleProductoCompra {
	
	public static daoDetalleProductoCompra _Instancia;
	private daoDetalleProductoCompra() {};
	public static daoDetalleProductoCompra Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoDetalleProductoCompra();
		}
		return _Instancia;
	}	
	
	
	public Boolean InsertarDetalleCompra(ArrayList<entDetalleProductoCompra> ledc) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		
		try {
			for(entDetalleProductoCompra edc : ledc) {
				CallableStatement cst = cn.prepareCall("{call InsertarDetalleCompra(?,?,?)}");
				cst.setInt(1, edc.getCantidad());
				cst.setInt(2, edc.getObjCompra().getIdCompra());
				cst.setInt(3, edc.getObjProducto().getIdProducto());
				
				int i = cst.executeUpdate();
				if(i<0) {inserto=true;}
			}			
			
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}
	
	public ArrayList<entDetalleProductoCompra> ListarDetalleProductoCompras() throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entDetalleProductoCompra> listaDetalleProductoCompras = new ArrayList<entDetalleProductoCompra>();
		try {
			CallableStatement cst = cn.prepareCall("{call ListarDetalleProductoCompra()}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){	
				entDetalleProductoCompra epc = new entDetalleProductoCompra();
					epc.setIdDetalleProductoCompra(rs.getInt("idDetalleProductoCompra"));
					epc.setCantidad(rs.getInt("Cantidad"));
					entCompra co = new entCompra();
						co.setIdCompra(rs.getInt("idCompra"));
						co.setCostoTotal(rs.getFloat("CostoTotal"));
						co.setFecha(rs.getDate("Fecha"));
					epc.setObjCompra(co);
					entProducto pro = new entProducto();
						pro.setIdProducto(rs.getInt("idProducto"));
						pro.setImagen(rs.getString("Imagen"));
						pro.setNombre(rs.getString("Nombre"));
						pro.setPrecio(rs.getDouble("Precio"));
					epc.setObjProducto(pro);
					listaDetalleProductoCompras.add(epc);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaDetalleProductoCompras;
	}
	

}
