package com.dao.comunes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;

import com.entidades.comunes.entCompra;
import com.entidades.comunes.entPedido;
import com.entidades.comunes.entProducto;
import com.entidades.comunes.entPromocion;
import com.entidades.comunes.entProveedor;
import com.entidades.comunes.entUsuario;

public class daoProveedor {
	
	public static daoProveedor _Instancia;
	private daoProveedor() {};
	public static daoProveedor Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoProveedor();
		}
		return _Instancia;
	}	
	
	
	public ArrayList<entProveedor> ListarProveedores() throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entProveedor> listaProveedores = new ArrayList<entProveedor>();
		try {
			CallableStatement cst = cn.prepareCall("{call ListarProveedores()}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				
				entProveedor objProveedor = new entProveedor();
					objProveedor.setIdProveedor(rs.getInt("idProveedor"));
					objProveedor.setNombre(rs.getString("Nombre"));
					objProveedor.setCelular(rs.getInt("Celular"));
					objProveedor.setDireccion(rs.getString("Direccion"));
					objProveedor.setCorreo(rs.getString("Correo"));
				listaProveedores.add(objProveedor);
					
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaProveedores;
	}
		
	public ArrayList<entProducto> ListarProductosPorProveedor(int idProveedor) throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entProducto> listaProductos = new ArrayList<entProducto>();
		try {
			CallableStatement cst = cn.prepareCall("{call ListarProductosProveedor(?)}");
			cst.setInt(1, idProveedor);
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				
				entProducto objProducto = new entProducto();
				
					objProducto.setNombre(rs.getString("Nombre"));
					objProducto.setImagen(rs.getString("Imagen"));
					objProducto.setIdProducto(rs.getInt("idProducto"));
					
					listaProductos.add(objProducto);
					
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaProductos;
	}
	
	public boolean InsertarProveedor(entProveedor objProveedor) throws Exception {
		Connection cn = Conexion.conectar();
		Boolean inserto = false;

		try {
			CallableStatement cst = cn.prepareCall("{call InsertarProveedor(?,?,?,?)}");
			cst.setString(1, objProveedor.getNombre());
			cst.setInt(2, objProveedor.getCelular());
			cst.setString(3, objProveedor.getDireccion());
			cst.setString(4, objProveedor.getCorreo());

			inserto = cst.execute();


		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
		}

		return inserto;
	}

}
