package com.dao.comunes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;


import com.entidades.comunes.entLinea;

import com.entidades.comunes.entProducto;

public class daoProducto {
		

	public static daoProducto _Instancia;
	private daoProducto() {}
	public static daoProducto Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoProducto();
		}
		return _Instancia;
	}	
	
	public ArrayList<entProducto> ListarProductos() throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entProducto> listaProductos = new ArrayList<entProducto>();
		try {
			CallableStatement cst = cn.prepareCall("{call ListarProductos()}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				entProducto p = new entProducto();
					p.setIdProducto(rs.getInt("idProducto"));
					p.setNombre(rs.getString("NombreProducto"));
					p.setPrecio(rs.getDouble("Precio"));
					p.setDescripcion(rs.getString("Descripcion"));
					p.setImagen(rs.getString("ImagenProducto"));
					entLinea l = new entLinea();
						l.setIdLinea(rs.getInt("idLinea"));
						l.setNombre(rs.getString("NombreLinea"));
						l.setImagen(rs.getString("ImagenLinea"));
					p.setObjLinea(l);
				listaProductos.add(p);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaProductos;
	}	
	
	
	public ArrayList<entProducto> ListarProductosLinea(int idLinea) throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entProducto> listaProductos = new ArrayList<entProducto>();
		try {
			CallableStatement cst = cn.prepareCall("{call ListarProductosLinea("+idLinea+")}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				entProducto p = new entProducto();
					p.setIdProducto(rs.getInt("idProducto"));
					p.setNombre(rs.getString("NombreProducto"));
					p.setPrecio(rs.getDouble("Precio"));
					p.setDescripcion(rs.getString("Descripcion"));
					p.setImagen(rs.getString("ImagenProducto"));
					entLinea l = new entLinea();
						l.setIdLinea(rs.getInt("idLinea"));
						l.setNombre(rs.getString("NombreLinea"));
						l.setImagen(rs.getString("ImagenLinea"));
					p.setObjLinea(l);
				listaProductos.add(p);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaProductos;
	}	



	public ArrayList<entProducto> ListarProductoss() throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entProducto> listaProducto = new ArrayList<entProducto>();
		try {
			CallableStatement cst = cn.prepareCall("{call sp_ListarProductos()}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				entProducto p = new entProducto();
				//p.setIdProducto(rs.getInt("idProducto"));
				p.setNombre(rs.getString("nombre"));
				p.setPrecio(rs.getDouble("precio"));	
				p.setDescripcion(rs.getString("descripcion"));
				p.setImagen(rs.getString("imagen"));
				listaProducto.add(p);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaProducto;
	}
	
	public Boolean InsertarProducto(entProducto p) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		try {
			CallableStatement cst = 
					cn.prepareCall("{call sp_InsertarProducto(?,?,?,?,?)}");
			cst.setInt(1, p.getObjLinea().getIdLinea());
			cst.setString(2, p.getNombre());
			cst.setDouble(3, p.getPrecio());
			cst.setString(4, p.getDescripcion());
			cst.setString(5, p.getImagen());
			int i = cst.executeUpdate();
			if(i>0) {inserto=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}
	
	public Boolean EliminarProducto(int idProducto) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		try {
			CallableStatement cst = 
					
			cn.prepareCall("{call EliminarProducto(?)}");
			cst.setInt(1, idProducto);
	
			int i = cst.executeUpdate();
			if(i<0) {inserto=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}	
	
	public Boolean EditarProducto(entProducto p) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		try {
			CallableStatement cst = cn.prepareCall("{call EditarProducto(?,?,?)}");
			cst.setInt(1, p.getIdProducto());
			cst.setDouble(2, p.getPrecio());
			cst.setString(3,p.getImagen());
			int i = cst.executeUpdate();
			if(i>0) {inserto=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}
	
	public ArrayList<entProducto> f_ListarProductos() throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entProducto> listaProducto = new ArrayList<entProducto>();
		try {
			CallableStatement cst = cn.prepareCall("{call sp_ListarProductos()}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				entProducto p = new entProducto();
				p.setIdProducto(rs.getInt("idProducto"));
				p.setNombre(rs.getString("nombre"));
				p.setPrecio(rs.getDouble("precio"));	
				p.setDescripcion(rs.getString("descripcion"));
				p.setImagen(rs.getString("imagen"));
				listaProducto.add(p);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaProducto;
	}
	public Boolean InsertarProductoss(entProducto p) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		try {
			CallableStatement cst = 
					cn.prepareCall("{call sp_InsertarProducto(?,?,?,?,?)}");
			cst.setInt(1, p.getObjLinea().getIdLinea());
			cst.setString(2, p.getNombre());
			cst.setDouble(3, p.getPrecio());
			cst.setString(4, p.getDescripcion());
			cst.setString(5, p.getImagen());
			int i = cst.executeUpdate();
			if(i>0) {inserto=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}
	
	public boolean p_deleteProductos(int idProducto) throws Exception {
		Connection cn = null;
		boolean eliminar = false;
		CallableStatement cstm = null;
		try {
			cn = Conexion.conectar();
			cstm = cn.prepareCall("{call sp_EliminarProducto(?)}");
			cstm.setInt(1,idProducto);
			int x = cstm.executeUpdate();
			if(x!=0){
				eliminar = true;
			}
			return eliminar;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
		}
	}
	
	
	
	public boolean p_UpdateProductos(entProducto p) throws Exception {
		Connection cn = null;
		boolean editar = false;
		CallableStatement cstm = null;
		try {		
			cn = Conexion.conectar();
			cstm = cn.prepareCall("{call sp_EditarProducto(?,?,?)}");	
			cstm.setInt(1, p.getIdProducto());
			cstm.setDouble(2, p.getPrecio());
			cstm.setString(3, p.getImagen());
			int x = cstm.executeUpdate();
			if(x!=0){
				editar = true;
			}
			return editar;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
		}
	}
	public entProducto DevolverProductoId(int id) throws Exception {
		Connection cn = null;
		entProducto p = null;
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = Conexion.conectar();
			cstm = cn.prepareCall("{call sp_listarProductoPorId(?,?,?,?,?)}");
			cstm.setInt(1, id);
			rs = cstm.executeQuery();
			if (rs.next()) {								
				p = new entProducto();
				p.setIdProducto(rs.getInt("idProducto"));
				p.setNombre(rs.getString("nombre"));
				p.setPrecio(rs.getDouble("precio"));
				p.setDescripcion(rs.getString("descripcion"));
				p.setImagen(rs.getString("imagen"));
			}
			return p;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
			rs.close();
		}
	}
	
}