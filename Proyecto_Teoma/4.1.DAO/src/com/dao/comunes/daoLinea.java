package com.dao.comunes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.entidades.comunes.entLinea;

public class daoLinea {	
	
	public static daoLinea _Instancia;
	private daoLinea() {};
	public static daoLinea Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoLinea();
		}
		return _Instancia;
	}	
	
	public ArrayList<entLinea> ListarLineas() throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entLinea> listaLinea = new ArrayList<entLinea>();
		try {
			CallableStatement cst = cn.prepareCall("{call ListarLineas()}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				entLinea l = new entLinea();
				l.setIdLinea(rs.getInt("idLinea"));
				l.setNombre(rs.getString("Nombre"));
				l.setImagen(rs.getString("Imagen"));				
				listaLinea.add(l);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaLinea;
	}
	
	public Boolean InsertarLineass(entLinea L) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		try {
			CallableStatement cst = cn.prepareCall("{call sp_InsertarLinea(?,?)}");
			cst.setString(1, L.getNombre());
			cst.setString(2,L.getImagen());
			int i = cst.executeUpdate();
			if(i>0) {inserto=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}
	
	public Boolean EliminarLinea(int idLinea) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		try {
			CallableStatement cst = 
					
			cn.prepareCall("{call EliminarLinea(?)}");
			cst.setInt(1, idLinea);
	
			int i = cst.executeUpdate();
			if(i<0) {inserto=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}	
	
	public Boolean EditarLinea(entLinea L) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		try {
			CallableStatement cst = cn.prepareCall("{call EditarLinea(?,?,?)}");
			cst.setInt(1, L.getIdLinea());
			cst.setString(2, L.getNombre());
			cst.setString(3,L.getImagen());
			int i = cst.executeUpdate();
			if(i>0) {inserto=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}
	
	public Boolean p_updateLineas(entLinea L) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean edito = false;
		try {
			CallableStatement cst = cn.prepareCall("{call sp_EditarLinea(?,?,?)}");
			cst.setInt(1,L.getIdLinea());
			cst.setString(2, L.getNombre());
			cst.setString(3,L.getImagen());
			int i = cst.executeUpdate();
			if(i!=0) {edito=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return edito;
	}
	public boolean p_deleteLineas(int idLinea) throws Exception {
		Connection cn = null;
		boolean eliminar = false;
		CallableStatement cstm = null;
		try {
			cn = Conexion.conectar();
			cstm = cn.prepareCall("{call sp_EliminarLinea(?)}");
			cstm.setInt(1,idLinea);
			int x = cstm.executeUpdate();
			if(x!=0){
				eliminar = true;
			}
			return eliminar;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
		}
	}

}
