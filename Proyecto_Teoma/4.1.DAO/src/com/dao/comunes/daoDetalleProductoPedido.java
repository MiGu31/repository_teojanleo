package com.dao.comunes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.annotation.Generated;

import com.entidades.comunes.entDetalleProductoPedido;
import com.entidades.comunes.entPedido;
import com.entidades.comunes.entPersona;
import com.entidades.comunes.entProducto;

public class daoDetalleProductoPedido {
	
	
	public static daoDetalleProductoPedido _Instancia;
	private daoDetalleProductoPedido() {};
	public static daoDetalleProductoPedido Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoDetalleProductoPedido();
		}
		return _Instancia;
	}	
	
	public Boolean InsertarDetallePedido(ArrayList<entDetalleProductoPedido> ledp) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		
		try {
			for(entDetalleProductoPedido edp : ledp) {
				CallableStatement cst = cn.prepareCall("{call InsertarDetallePedido(?,?,?)}");
				cst.setInt(1, edp.getCantidad());
				cst.setInt(2, edp.getObjPedido().getIdPedido());
				cst.setInt(3, edp.getObjProducto().getIdProducto());
				
				int i = cst.executeUpdate();
				if(i<0) {inserto=true;}
			}			
			
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}
	
	
	public ArrayList<entDetalleProductoPedido> ListarDetalleProductoPedidos() throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entDetalleProductoPedido> listaDetalleProductoPedidos = new ArrayList<entDetalleProductoPedido>();
		try {
			CallableStatement cst = cn.prepareCall("{call ListarDetalleProductoPedido()}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){	
				entDetalleProductoPedido dpp = new entDetalleProductoPedido();
					dpp.setIdDetalleProductoPedido(rs.getInt("idDetalleProducto"));
					dpp.setCantidad(rs.getInt("Cantidad"));
					entPedido pe = new entPedido();
						pe.setIdPedido(rs.getInt("idPedido"));
						pe.setCostoTotal(rs.getFloat("CostoTotal"));
						pe.setFecha(rs.getDate("Fecha"));
					dpp.setObjPedido(pe);
					entProducto pro = new entProducto();
						pro.setIdProducto(rs.getInt("idProducto"));
						pro.setImagen(rs.getString("Imagen"));
						pro.setNombre(rs.getString("Nombre"));
						pro.setPrecio(rs.getDouble("Precio"));
					dpp.setObjProducto(pro);
					listaDetalleProductoPedidos.add(dpp);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaDetalleProductoPedidos;
	}

}
