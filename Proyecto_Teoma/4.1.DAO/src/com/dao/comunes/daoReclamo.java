package com.dao.comunes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.entidades.comunes.entPersona;
import com.entidades.comunes.entReclamo;
import com.entidades.comunes.entUsuario;

public class daoReclamo {
	
	public static daoReclamo _Instancia;
	private daoReclamo() {};
	public static daoReclamo Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoReclamo();
		}
		return _Instancia;
	}	
	
	public ArrayList<entReclamo> ListarReclamos() throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entReclamo> listaReclamos = new ArrayList<entReclamo>();
		try {
			CallableStatement cst = cn.prepareCall("{call ListarReclamos()}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				entReclamo r = new entReclamo();
					r.setIdReclamo(rs.getInt("idReclamo"));
					r.setMotivo(rs.getString("Motivo"));
					r.setDescripcion(rs.getString("Descripcion"));
					r.setFecha(rs.getDate("Fecha"));
					r.setEstado(rs.getBoolean("Estado"));
					entUsuario u = new entUsuario();
						u.setIdUsuario(rs.getInt("idUsuario"));
						u.setUsername(rs.getString("Username"));
						u.setEmail(rs.getString("Email"));
						u.setTipoUsuario(rs.getString("TipoUsuario"));
					r.setObjUsuario(u);					
				listaReclamos.add(r);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaReclamos;
	}
	
	public Boolean InsertarReclamo(entReclamo r) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		try {
			CallableStatement cst = cn.prepareCall("{call InsertarReclamo(?,?,?)}");
			cst.setString(1, r.getMotivo());
			cst.setString(2, r.getDescripcion());
			cst.setInt(3, r.getObjUsuario().getIdUsuario());
			int i = cst.executeUpdate();
			if(i<0) {inserto=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}
	
	public Boolean CambiarEstado(int idReclamo) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		try {
			CallableStatement cst = 
					
			cn.prepareCall("{call EliminarReclamo(?)}");
			cst.setInt(1, idReclamo);
	
			int i = cst.executeUpdate();
			if(i<0) {inserto=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}	
}
