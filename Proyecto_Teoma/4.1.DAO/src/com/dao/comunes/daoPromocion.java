package com.dao.comunes;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.entidades.comunes.entPromocion;
import com.entidades.comunes.entUsuario;

public class daoPromocion {	
	
	public static daoPromocion _Instancia;
	private daoPromocion() {};
	public static daoPromocion Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoPromocion();
		}
		return _Instancia;
	}	
	
	public ArrayList<entPromocion> ListarPromociones() throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entPromocion> listaPromociones = new ArrayList<entPromocion>();
		try {
			CallableStatement cst = cn.prepareCall("{call ListarPromociones()}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				entPromocion p = new entPromocion();
					p.setIdPromocion(rs.getInt("idPromocion"));
					p.setNombre(rs.getString("Nombre"));
					p.setFechaCreacion(rs.getDate("FechaCreacion"));
					p.setFechaCaducidad(rs.getDate("FechaCaducidad"));
					p.setDescripcion(rs.getString("Descripcion"));
					p.setValor(rs.getDouble("Valor"));
				listaPromociones.add(p);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaPromociones;
	}
	
	public Boolean InsertarPromocion(entPromocion p) throws Exception{
		//String rptaBD ="";
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		try {
			CallableStatement cst = cn.prepareCall("{call InsertarPromocion(?,?,?,?)}");
			cst.setString(1, p.getNombre());
			cst.setString(2, p.getDescripcion());
			cst.setDate	 (3, p.getFechaCaducidad());
			cst.setDouble(4, p.getValor());
			int i = cst.executeUpdate();
			if(i<0) {inserto=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}
	
	
	public Boolean EliminarPromocion(int idPromocion) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		try {
			CallableStatement cst = 
					
			cn.prepareCall("{call EliminarPromocion(?)}");
			cst.setInt(1, idPromocion);
	
			int i = cst.executeUpdate();
			if(i<0) {inserto=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}
	
	public entPromocion ObtenerPromocion() throws Exception{
		entPromocion objPromocion = new entPromocion();
	    Statement stmt = null;
		String Query = "SELECT idPromocion, Nombre, Descripcion, FechaCreacion, FechaCaducidad, Valor FROM Promocion WHERE Estado=1";

		Connection cn = Conexion.conectar();
		try {
			stmt = cn.createStatement();
		    ResultSet rs = stmt.executeQuery(Query);
			//CallableStatement cst = cn.prepareCall("{call ObtenerPromocion()}");
			while(rs.next()){
				objPromocion.setIdPromocion(rs.getInt("idPromocion"));
				objPromocion.setNombre(rs.getString("Nombre"));
				objPromocion.setFechaCreacion(rs.getDate("FechaCreacion"));
				objPromocion.setFechaCaducidad(rs.getDate("FechaCaducidad"));
				objPromocion.setDescripcion(rs.getString("Descripcion"));
				objPromocion.setValor(rs.getDouble("Valor"));		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return objPromocion;
	}

	
}
