package com.dao.comunes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;

import com.entidades.comunes.entLinea;
import com.entidades.comunes.entPedido;
import com.entidades.comunes.entPromocion;
import com.entidades.comunes.entUsuario;

public class daoPedido {
	
	public static daoPedido _Instancia;
	private daoPedido() {};
	public static daoPedido Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoPedido();
		}
		return _Instancia;
	}	
	
	public boolean InsertarPedido(entPedido p) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		
		try {
			CallableStatement cst = cn.prepareCall("{call InsertarPedido(?,?,?)}");
			cst.setInt(1, p.getObjUsuario().getIdUsuario());
			cst.setFloat(2, p.getCostoTotal());
			cst.registerOutParameter(3, Types.INTEGER);
			
			inserto = cst.execute();
			
			if(!inserto) {
				p.setIdPedido(cst.getInt("id"));
			}
			
		} catch (Exception e) {
			throw e;
		}finally {
			cn.close();
		}
		
		return inserto;		
		
	}
	
	public ArrayList<entPedido> ListarPedidos(int idUsuario) throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entPedido> listaPedidos = new ArrayList<entPedido>();
		try {
			CallableStatement cst = cn.prepareCall("{call ListarPedidos("+idUsuario+")}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				entPedido p = new entPedido();
					p.setIdPedido(rs.getInt("idPedido"));
					p.setFecha(rs.getDate("Fecha"));
					p.setCostoTotal(rs.getFloat("CostoTotal"));		
					entPromocion prom = new entPromocion();
						prom.setNombre(rs.getString("Nombre"));
					p.setObjPromocion(prom);
				listaPedidos.add(p);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaPedidos;
	}
	
	public ArrayList<entPedido> ListarPedidosGenerales() throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entPedido> listaPedidosGenerales = new ArrayList<entPedido>();
		try {
			CallableStatement cst = cn.prepareCall("{call ListarPedidosGenerales()}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				entPedido p = new entPedido();
					p.setIdPedido(rs.getInt("idPedido"));
					p.setFecha(rs.getDate("Fecha"));
					p.setCostoTotal(rs.getFloat("CostoTotal"));		
					entUsuario u = new entUsuario();
						u.setUsername(rs.getString("Username"));
					p.setObjUsuario(u);
					entPromocion pro = new entPromocion();
						pro.setNombre(rs.getString("NombrePromocion"));
					p.setObjPromocion(pro);
					listaPedidosGenerales.add(p);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaPedidosGenerales;
	}

}
