package com.dao.comunes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.ArrayList;

import com.entidades.comunes.entDetalleProductoPedido;
import com.entidades.comunes.entDetalleProveedorProducto;
import com.entidades.comunes.entProducto;
import com.entidades.comunes.entProveedor;

public class daoDetalleProveedorProducto {
	
	public static daoDetalleProveedorProducto _Instancia;
	private daoDetalleProveedorProducto() {};
	public static daoDetalleProveedorProducto Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoDetalleProveedorProducto();
		}
		return _Instancia;
	}	
	
	
	public Boolean InsertarDetalleProveedorProducto(entProveedor objProveedor, entProducto objProducto) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		
		try {
				CallableStatement cst = cn.prepareCall("{call InsertarDetalleProveedorProducto(?,?)}");
				cst.setInt(1, objProveedor.getIdProveedor());
				cst.setInt(2, objProducto.getIdProducto());
				
				int i = cst.executeUpdate();
				if(i<0) {inserto=true;}
			
			
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}

}
