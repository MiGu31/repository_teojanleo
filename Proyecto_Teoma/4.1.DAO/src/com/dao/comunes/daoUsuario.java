package com.dao.comunes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.entidades.comunes.entPersona;
import com.entidades.comunes.entProducto;
import com.entidades.comunes.entUsuario;

public class daoUsuario {
	
	public static daoUsuario _Instancia;
	private daoUsuario() {};
	public static daoUsuario Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoUsuario();
		}
		return _Instancia;
	}	
	
	public entUsuario VerificarAcceso(String Username, String Password) throws Exception{
		Connection cn = Conexion.conectar();
		entUsuario u = null;
		try {
			CallableStatement cst = cn.prepareCall("{call VerificarAcceso(?,?)}");
			cst.setString(1, Username);
			cst.setString(2, Password);
			ResultSet rs = cst.executeQuery(); 
			if(rs.next()){
				u = new entUsuario();				
				u.setIdUsuario(rs.getInt("idUsuario"));
				u.setUsername(rs.getString("Username"));
				u.setPassword(rs.getString("Password"));
				u.setTipoUsuario(rs.getString("TipoUsuario"));
				u.setFechaCaducidad(rs.getDate("FechaCaducidad"));
				u.setEstado(rs.getBoolean("Estado"));
				u.setEmail(rs.getString("Email"));
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return u;
	}
	
	public Boolean CambiarEstado(int idUsuario) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		try {
			CallableStatement cst = 
					
			cn.prepareCall("{call CambiarEstado(?)}");
			cst.setInt(1, idUsuario);
	
			int i = cst.executeUpdate();
			if(i<0) {inserto=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}	
	
	public ArrayList<entUsuario> ListarUsuarios() throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entUsuario> listaUsuarios = new ArrayList<entUsuario>();
		try {
			CallableStatement cst = cn.prepareCall("{call ListarUsuarios()}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				entUsuario objUsuario = new entUsuario();
					objUsuario.setUsername(rs.getString("Username"));
					objUsuario.setEmail(rs.getString("Email"));
					objUsuario.setTipoUsuario(rs.getString("TipoUsuario"));
					objUsuario.setEstado(rs.getBoolean("Estado"));
				listaUsuarios.add(objUsuario);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaUsuarios;
	}
	
}
