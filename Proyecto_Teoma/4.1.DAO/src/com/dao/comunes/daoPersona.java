package com.dao.comunes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.entidades.comunes.entLinea;

import com.entidades.comunes.entPersona;
import com.entidades.comunes.entUsuario;

public class daoPersona {
	

	public static daoPersona _Instancia;
	private daoPersona() {}
	public static daoPersona Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoPersona();
		}
		return _Instancia;
	}
	
	
	public Boolean InsertarCliente(entPersona p) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		try {
			CallableStatement cst = cn.prepareCall("{call InsertarCliente(?,?,?,?,?,?,?,?,?)}");
			cst.setString(1, p.getObjUsuario().getUsername());
			cst.setString(2, p.getObjUsuario().getPassword());
			cst.setString(3, p.getObjUsuario().getEmail());
			
			cst.setString(4, p.getDni());
			cst.setString(5, p.getNombres());
			cst.setString(6, p.getApellidos());
			cst.setString(7, p.getTelefonoCelular());
			cst.setString(8, p.getTelefonoFijo());
			cst.setString(9, p.getDireccion());

			
			if(cst.executeUpdate()<0) {inserto=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}
	
	public ArrayList<entPersona> ListarClientes() throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entPersona> listaClientes = new ArrayList<entPersona>();
		try {
			CallableStatement cst = cn.prepareCall("{call ListarClientes()}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				entPersona p = new entPersona();
					p.setIdPersona(rs.getInt("idPersona"));
					p.setDni(rs.getString("Dni"));
					p.setNombres(rs.getString("Nombres"));
					p.setApellidos(rs.getString("Apellidos"));
					p.setTelefonoCelular(rs.getString("TelefonoCelular"));
					p.setTelefonoFijo(rs.getString("TelefonoFijo"));
					p.setDireccion(rs.getString("Direccion"));
					entUsuario u = new entUsuario();
						u.setIdUsuario(rs.getInt("idUsuario"));
						u.setUsername(rs.getString("Username"));
						u.setEmail(rs.getString("Email"));
						u.setTipoUsuario(rs.getString("TipoUsuario"));
						u.setEstado(rs.getBoolean("Estado"));
					p.setObjUsuario(u);						
				listaClientes.add(p);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaClientes;
	}
	
	public ArrayList<entPersona> ListarPersonas() throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entPersona> listaPersona = new ArrayList<entPersona>();
		try {
			CallableStatement cst = cn.prepareCall("{call sp_ListarPersonas()}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				entPersona p = new entPersona();
				entUsuario u = new entUsuario();
				p.setIdPersona(rs.getInt("idPersona"));
				p.setDni(rs.getString("dni"));
				p.setNombres(rs.getString("nombres"));
				p.setApellidos(rs.getString("apellidos"));	
				p.setTelefonoCelular(rs.getString("telefonoCelular"));
				p.setTelefonoFijo(rs.getString("telefonoFijo"));
				u.setIdUsuario(rs.getInt("idUsuario"));
				p.setObjUsuario(u);
				listaPersona.add(p);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaPersona;
	}
	
	public ArrayList<entPersona> p_ListarPersonas() throws Exception{
		Connection cn = Conexion.conectar();
		ArrayList<entPersona> listaPersona = new ArrayList<entPersona>();
		try {
			CallableStatement cst = cn.prepareCall("{call sp_ListarPersonas()}");
			ResultSet rs = cst.executeQuery();
			while(rs.next()){
				entPersona p = new entPersona();
				p.setIdPersona(rs.getInt("idPersona"));
				p.setDni(rs.getString("Dni"));
				p.setNombres(rs.getString("Nombres"));
				p.setApellidos(rs.getString("Apellidos"));	
				p.setTelefonoCelular(rs.getString("TelefonoCelular"));
				p.setTelefonoFijo(rs.getString("TelefonoFijo"));
				p.setDireccion(rs.getString("Direccion"));
				listaPersona.add(p);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listaPersona;
	}
	
	public Boolean InsertarPersonass(entPersona p) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		try {
			CallableStatement cst = 
					cn.prepareCall("{call sp_InsertarPersona(?,?,?,?,?,?,?)}");
			cst.setString(1, p.getDni());
			cst.setString(2, p.getNombres());
			cst.setString(3, p.getApellidos());
			cst.setString(4, p.getTelefonoCelular());
			cst.setString(5, p.getTelefonoFijo());
			cst.setString(6, p.getDireccion());
			cst.setInt(7, p.getObjUsuario().getIdUsuario());
			int i = cst.executeUpdate();
			if(i>0) {inserto=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}
	public boolean p_UpdatePersonas(entPersona p) throws Exception {
		Connection cn = null;
		boolean editar = false;
		CallableStatement cst = null;
		try {		
			cn = Conexion.conectar();
			cst = cn.prepareCall("{call sp_EditarPersona(?,?,?,?,?,?,?)}");	
			cst.setInt(1, p.getIdPersona());
			cst.setString(2, p.getDni());
			cst.setString(3, p.getNombres());
			cst.setString(4, p.getApellidos());
			cst.setString(5, p.getTelefonoCelular());
			cst.setString(6, p.getTelefonoFijo());
			cst.setString(7, p.getDireccion());
			int x = cst.executeUpdate();
			if(x!=0){
				editar = true;
			}
			return editar;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cst.close();
		}
	}
	public boolean p_deletePersonas(int idPersona) throws Exception {
		Connection cn = null;
		boolean eliminar = false;
		CallableStatement cstm = null;
		try {
			cn = Conexion.conectar();
			cstm = cn.prepareCall("{call sp_EliminarPersona(?)}");
			cstm.setInt(1,idPersona);
			int x = cstm.executeUpdate();
			if(x!=0){
				eliminar = true;
			}
			return eliminar;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
		}
	}
	
	public Boolean EditarPersona(entPersona p) throws Exception{
		Connection cn = Conexion.conectar();
		Boolean inserto = false;
		try {
			CallableStatement cst = cn.prepareCall("{call EditarUsuario(?,?,?,?,?,?)}");
			cst.setInt(1, p.getObjUsuario().getIdUsuario());
			cst.setString(2, p.getObjUsuario().getPassword());
			cst.setString(3, p.getObjUsuario().getEmail());
			cst.setString(4, p.getTelefonoCelular());
			cst.setString(5, p.getTelefonoFijo());
			cst.setString(6, p.getDireccion());		
		
			int i = cst.executeUpdate();
			if(i>0) {inserto=true;}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return inserto;
	}	
	
}
