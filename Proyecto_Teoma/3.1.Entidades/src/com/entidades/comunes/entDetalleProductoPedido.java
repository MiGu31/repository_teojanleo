package com.entidades.comunes;

public class entDetalleProductoPedido {
	
	private int idDetalleProductoPedido;
	private int Cantidad;
	private entPedido objPedido;
	private entProducto objProducto;
	public int getIdDetalleProductoPedido() {
		return idDetalleProductoPedido;
	}
	public void setIdDetalleProductoPedido(int idDetalleProductoPedido) {
		this.idDetalleProductoPedido = idDetalleProductoPedido;
	}
	public int getCantidad() {
		return Cantidad;
	}
	public void setCantidad(int cantidad) {
		Cantidad = cantidad;
	}
	public entPedido getObjPedido() {
		return objPedido;
	}
	public void setObjPedido(entPedido objPedido) {
		this.objPedido = objPedido;
	}
	public entProducto getObjProducto() {
		return objProducto;
	}
	public void setObjProducto(entProducto objProducto) {
		this.objProducto = objProducto;
	}
	
	

}
