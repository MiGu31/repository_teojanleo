package com.entidades.comunes;

import java.sql.Date;

public class entUsuario {
	private int idUsuario;
	private String TipoUsuario;
	private String Username;
	private String Password;
	private Boolean Estado;
	private Date FechaCreacion;
	private Date FechaCaducidad;
	private String Email;
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getTipoUsuario() {
		return TipoUsuario;
	}
	public void setTipoUsuario(String tipoUsuario) {
		TipoUsuario = tipoUsuario;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public Boolean getEstado() {
		return Estado;
	}
	public void setEstado(Boolean estado) {
		Estado = estado;
	}
	public Date getFechaCreacion() {
		return FechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		FechaCreacion = fechaCreacion;
	}
	public Date getFechaCaducidad() {
		return FechaCaducidad;
	}
	public void setFechaCaducidad(Date fechaCaducidad) {
		FechaCaducidad = fechaCaducidad;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	
	

}
