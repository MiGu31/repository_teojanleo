package com.entidades.comunes;

import java.sql.Date;

public class entPromocion {
	
	private int idPromocion;
	private String Nombre;
	private String Descripcion;
	private Date FechaCreacion;
	private Date FechaCaducidad;
	private double Valor;
	
	public int getIdPromocion() {
		return idPromocion;
	}
	public void setIdPromocion(int idPromocion) {
		this.idPromocion = idPromocion;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public Date getFechaCreacion() {
		return FechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		FechaCreacion = fechaCreacion;
	}
	public Date getFechaCaducidad() {
		return FechaCaducidad;
	}
	public void setFechaCaducidad(Date fechaCaducidad) {
		FechaCaducidad = fechaCaducidad;
	}
	public double getValor() {
		return Valor;
	}
	public void setValor(double valor) {
		Valor = valor;
	}
	
	

}
