package com.entidades.comunes;

import java.sql.Date;
import java.util.ArrayList;

public class entCompra {
	
	private int idCompra;
	private float CostoTotal;
	private Date Fecha;
	private entUsuario objUsuario;
	private entProveedor objProveedor;
	private ArrayList<entDetalleProductoCompra> ListaDetalle;
	
	public int getIdCompra() {
		return idCompra;
	}
	public void setIdCompra(int idCompra) {
		this.idCompra = idCompra;
	}
	public Date getFecha() {
		return Fecha;
	}
	public void setFecha(Date fecha) {
		Fecha = fecha;
	}
	
	public entUsuario getObjUsuario() {
		return objUsuario;
	}
	public void setObjUsuario(entUsuario objUsuario) {
		this.objUsuario = objUsuario;
	}
	public ArrayList<entDetalleProductoCompra> getListaDetalle() {
		return ListaDetalle;
	}
	public void setListaDetalle(ArrayList<entDetalleProductoCompra> listaDetalle) {
		ListaDetalle = listaDetalle;
	}
	
	public float getCostoTotal() {
		return CostoTotal;
	}
	public void setCostoTotal(float costoTotal) {
		CostoTotal = costoTotal;
	}
	
	public entProveedor getObjProveedor() {
		return objProveedor;
	}
	public void setObjProveedor(entProveedor objProveedor) {
		this.objProveedor = objProveedor;
	}	

}
