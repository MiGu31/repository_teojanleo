package com.entidades.comunes;

public class entDetalleProveedorProducto {
	
	private int idDetalleProveedorProducto;
	private entProveedor objProveedor;
	private entProducto objProducto;
	public int getIdDetalleProveedorProducto() {
		return idDetalleProveedorProducto;
	}
	public void setIdDetalleProveedorProducto(int idDetalleProveedorProducto) {
		this.idDetalleProveedorProducto = idDetalleProveedorProducto;
	}
	public entProveedor getObjProveedor() {
		return objProveedor;
	}
	public void setObjProveedor(entProveedor objProveedor) {
		this.objProveedor = objProveedor;
	}
	public entProducto getObjProducto() {
		return objProducto;
	}
	public void setObjProducto(entProducto objProducto) {
		this.objProducto = objProducto;
	}
	
		
	
}
