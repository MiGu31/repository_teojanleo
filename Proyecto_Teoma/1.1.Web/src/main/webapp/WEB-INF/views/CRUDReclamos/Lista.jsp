<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/DT_bootstrap.css" var="dtbootstrapcss"/>
<link href="${dtbootstrapcss}" rel="stylesheet"/>


<jsp:include page="/WEB-INF/views/PaginaMaestra/Administrador/AdmiHeader.jsp"></jsp:include>
              <!-- page start-->
              <div class="row">
                <div class="col-sm-12">
              <section class="panel">
              <header class="panel-heading">
                  Reclamos hasta la fecha
             <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span>
              </header>
              <div class="panel-body">
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id="dynamic-table">
              <thead>
              <tr>
                  <th>Motivo</th>
                  <th>Fecha</th>
                  <th>Usuario</th>
                  <th>Correo electrónico</th>
                  <td align="center"><strong>Descripcion</strong></td>
                  <td align="center"><strong>Verificar</strong></td>
              </tr>
              </thead>
              <tbody>  
              
              	<c:forEach items="${listaReclamos}" var="reclamo">     
	              <tr class="gradeX">
	                  <td>${reclamo.motivo}</td>
	                  <td>${reclamo.fecha}</td>
	                  <td>${reclamo.objUsuario.username}</td>
	                  <td>${reclamo.objUsuario.email}</td>
					  <td align="center"> 
					  	<a class="btn btn-success" data-toggle="modal" href="#modalDescripcionReclamo-${reclamo.idReclamo}">
                                  Ver descripcion                         
                        </a>                        
                      </td>   
	                  <frm:form method="GET" action="${pageContext.request.contextPath}/CRUDReclamos/Lista">
							<td align="center"><button name="idReclamo" value="${reclamo.idReclamo} type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button></td>                                							
				      </frm:form>    

                      
                      
                        <div class="modal fade modal-dialog-center " id="modalDescripcionReclamo-${reclamo.idReclamo}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog ">
                                <div class="modal-content-wrap">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Descripcion del reclamo</h4>
                                        </div>
                                        <div class="modal-body">

                                            ${reclamo.descripcion}

                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-default" type="button">Cerrar</button>                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                							
	              </tr>  		
				</c:forEach>  

              </tfoot>
              </table>
              </div>
              </div>
              </section>
       </div>
</div>


<jsp:include page="/WEB-INF/views/PaginaMaestra/Administrador/AdmiFooter.jsp"></jsp:include>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/js/jquery-ui-1.9.2.custom.min.js" var="jqueryui"/>
<script src="${jqueryui}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/DT_bootstrap.js" var="dtbootstrapjs"/>
<script src="${dtbootstrapjs}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.dataTables.js" var="jquerydatablejs"/>
<script src="${jquerydatablejs}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/dynamic_table_init.js" var="dynamictablejs"/>
<script src="${dynamictablejs}"></script>

