<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/DT_bootstrap.css" var="dtbootstrapcss"/>
<link href="${dtbootstrapcss}" rel="stylesheet"/>

<jsp:include page="/WEB-INF/views/PaginaMaestra/Usuario/UserHeader.jsp"></jsp:include>

        <div class="col-lg-6">
                      <section class="panel">
                          <header class="panel-heading">
                              Registra tu reclamo
                          </header>
                          <div class="panel-body">
                            <frm:form method="POST" class="form-horizontal" action="${pageContext.request.contextPath}/CRUDReclamos/Insertar">
                                  <div class="form-group">
                                      <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Motivo</label>
                                      <div class="col-lg-10">
                                          <frm:input path="motivo" type="text" class="form-control" placeholder="Motivo"/>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label for="inputPassword1" class="col-lg-2 col-sm-2 control-label">Descripcion</label>
                                      <div class="col-lg-10">
                                           <frm:input path="descripcion" type="text" class="form-control" placeholder="Descripcion"/>
                                      </div>
                                  </div>
          
                                  <div class="form-group">
                                      <div class="col-lg-offset-2 col-lg-10">
                                          <button align="center" type="submit" class="btn btn-primary">Registrar</button>
                                      </div>
                                  </div>
                              </frm:form>
                          </div>
                      </section>
              </div>


<jsp:include page="/WEB-INF/views/PaginaMaestra/Usuario/UserFooter.jsp"></jsp:include>
