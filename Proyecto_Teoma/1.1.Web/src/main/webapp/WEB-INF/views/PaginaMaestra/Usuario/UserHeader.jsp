<%@page import="com.presentacion.pojo.entUsuario"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/icn/teoma-icon.ico" var="teoicn"/>
<link href="${teoicn}" rel="shortcut icon"/>

<title>Intranet Cliente</title>

	<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/assets/font-awesome/css/font-awesome.css" var="fontcss"/>
    <link href="${fontcss}" rel="stylesheet"/>

    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/bootstrap.min.css" var="mincss"/>
    <link href="${mincss}" rel="stylesheet"/>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/bootstrap-reset.css" var="resetcss"/>
    <link href="${resetcss}" rel="stylesheet"/>   
        
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/font-awesome.css" var="fontawecss"/>
    <link href="${fontawecss}" rel="stylesheet"/>

    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/jquery.steps.css" var="stepcss"/>
    <link href="${stepcss}" rel="stylesheet"/>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/slidebars.css" var="slidebarscss"/>
    <link href="${slidebarscss}" rel="stylesheet"/>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/style-responsive.css" var="stylerespcss"/>
    <link href="${stylerespcss}" rel="stylesheet"/>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/style.css" var="stylecss"/>
    <link href="${stylecss}" rel="stylesheet"/>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/jquery.easy-pie-chart.css" var="jquerychart"/>
    <link href="${jquerychart}" rel="stylesheet" media="screen"/>      
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/owl.carousel.css" var="carousel"/>
    <link href="${carousel}" rel="stylesheet"/> 

</head>
<body>

<section id="container">

<header class="header white-bg">
              <div class="sidebar-toggle-box">
                  <i class="fa fa-bars"></i>
              </div>
            <!--logo start-->
            <a href="#" class="logo">TEO<span>MA</span></a>
            <!--logo end-->
            
            <div class="top-nav ">
                <!--search & user info start-->
                <ul class="nav pull-right top-menu">
                    <li>
                        <input type="text" class="form-control search" placeholder="Search">
                    </li>
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <% entUsuario user=(entUsuario)session.getAttribute("objUsuario");%>
                            <%=user.getUsername() %>
                            <span class="username"><c:out value="${sessionScope.user.username}"/></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <!--  <li><a href="#"><i class=" fa fa-suitcase"></i>Perfil</a></li>-->
                            <div align="center"> 
                            <li><a href="${pageContext.request.contextPath}/CRUDUsuario/EditarUser"><i class="fa fa-cog"></i> Editar</a></li>
                             </div>
                            <!--  <li><a href="#"><i class="fa fa-bell-o"></i> Notificaciones</a></li>-->
                            <li><a href="${pageContext.request.contextPath}/CerrarSesion"><i class="fa fa-key"></i> Cerrar sesión</a></li>
                        </ul>
                    </li>
                    <li class="sb-toggle-right">
                        <i class="fa  fa-align-right"></i>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!--search & user info end-->
            </div>
        </header>       
        
        
        <!-- INICIO DEL MENU -->       
        
        <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                  <li>
                      <a class="active" href="${pageContext.request.contextPath}/Home/IntranetUsuario">
                          <i class="fa fa-tachometer"></i>
                          <span>Principal</span>
                      </a>
                  </li>    
                  
                 <li class="sub-menu">
                 <a href="javascript:;" >
                          <i class="fa fa-male"></i>
                          <span>Pedido</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="${pageContext.request.contextPath}/COREPedido/Insertar">Realizar pedido</a></li>                                                 
                      </ul>
                      <ul class="sub">
                          <li><a  href="${pageContext.request.contextPath}/COREPedido/ListaUsuario">Reporte de pedidos</a></li>                                                 
                      </ul>
                  </li>     
                  
                 <li class="sub-menu">
                 <a href="javascript:;" >
                          <i class="fa fa-comment-o"></i>
                          <span>Reclamos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="${pageContext.request.contextPath}/CRUDReclamos/Insertar">Registra tu reclamo</a></li>
                      </ul>
                  </li>       

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      
      <section id="main-content">
          <section class="wrapper site-min-height">
