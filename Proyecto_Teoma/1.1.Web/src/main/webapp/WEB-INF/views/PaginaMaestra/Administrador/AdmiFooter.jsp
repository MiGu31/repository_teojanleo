 <%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
 
  </section>
      </section>

</section>


   	<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.js" var="jqueryjs"/>
    <script src="${jqueryjs}"></script>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/bootstrap.min.js" var="bootjs"/>
    <script src="${bootjs}"></script>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.dcjqaccordion.2.7.js" var="accordionjs"/>
    <script class="include" src="${accordionjs}"></script>      
        
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.scrollTo.min.js" var="jquerynicescrolljs"/>
    <script src="${jquerynicescrolljs}"></script>   
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.nicescroll.js" var="jquerynicejs"/>
    <script src="${jquerynicejs}"></script>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.sparkline.js" var="jquertsparkjs"/>
    <script src="${jquertsparkjs}"></script>        
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.easy-pie-chart.js" var="jquerypiejs"/>
    <script src="${jquerypiejs}"></script>                           
                       
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/owl.carousel.js" var="owcarejs"/>
    <script src="${owcarejs}"></script>       
        
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.customSelect.min.js" var="customjs"/>
    <script src="${customjs}"></script>   
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/respond.min.js" var="respondjs"/>
    <script src="${respondjs}"></script>       
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/bootstrap-validator.min.js" var="validatorjs"/>
    <script src="${validatorjs}"></script>        

       
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.steps.min.js" var="jquerystepsjs"/>
    <script src="${jquerystepsjs}"></script>     
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.stepy.js" var="jquerystepyjs"/>
    <script src="${jquerystepyjs}"></script>         
        
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.validate.min.js" var="jqueryvalijs"/>
    <script src="${jqueryvalijs}"></script> 

    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/slidebars.min.js" var="slidejs"/>
    <script src="${slidejs}"></script> 
    
   <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/sparkline-chart.js" var="sparkchartjs"/>
   <script src="${sparkchartjs}"></script>
   
   <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/easy-pie-chart.js" var="easychartjs"/>
   <script src="${easychartjs}"></script>
   
   <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/count.js" var="countjs"/>
   <script src="${countjs}"></script>

	  <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/common-scripts.js" var="commonjs"/>
    <script src="${commonjs}"></script>  


</body>
</html>