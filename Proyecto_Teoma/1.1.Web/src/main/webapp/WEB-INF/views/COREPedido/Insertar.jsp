<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/DT_bootstrap.css" var="dtbootstrapcss"/>
<link href="${dtbootstrapcss}" rel="stylesheet"/>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Shop/css/bootstrap.min.css" var="mincss"/>
<link href="${mincss}" rel="stylesheet"/>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Shop/css/shop-homepage.css" var="shopcss"/>
<link href="${shopcss}" rel="stylesheet"/> 

<jsp:include page="/WEB-INF/views/PaginaMaestra/Usuario/UserHeader.jsp"></jsp:include>


<div class="row">

            <div class="col-md-3">
          
                <section class="panel">
                    <header class="panel-heading">
                        Categor�as
                    </header>
                    <div class="panel-body">
                        <ul class="nav prod-cat">                       
                            <c:forEach items="${listaLineas}" var="linea">   
                                 <li><a style="cursor: pointer;" class="btn" id="${linea.idLinea}"><i class=" fa fa-angle-right"></i> ${linea.nombre}</a></li>                                  				
							</c:forEach> 

                        </ul>
                    </div>                    
                </section>
                <div align="center">
						<button class="btn btn-primary" id="addVenta">Realizar Venta</button>
					</div> 
            </div>   

            <div class="col-md-9">             
                <section class="panel">                	
                  
                	<div id="parent">
		              	<c:forEach items="${listaProductos}" var="listP">   
		              		<div class="${listP.objLinea.idLinea}">
			              		<div class="${listP.objLinea.nombre}">	
								    <div class="col-sm-4 col-lg-4 col-md-4">
					                      	<div class="thumbnail ">
					                      	<div class="pro-img-box">
					                           <img src="${listP.imagen}" alt=""  height="150" width="320">
					                         </div>
					                           <div>
					                               <h4 class="pull-right" id="idPrecio-${listP.idProducto}">${listP.precio}</h4>
					                               <h4  id="idNombre-${listP.idProducto}">${listP.nombre}</h4>				                               				                                        
	
					                           </div>
					                           <div class="ratings">
					                               Cantidad: <input id="idcantidad-${listP.idProducto}" type="number" min="0" oninput="this.value = Math.abs(this.value)"/>
					                           </div>
					                           </br>
	
												<div align="center">
													<button class="agregar" id="${listP.idProducto}"><i class="fa fa-shopping-cart"></i>Agregar</button>
					                 			</div>
					                          </div>
					                  </div> 
				                  </div>   
			                  </div>   
  	
			                  	        
						</c:forEach>
					</div>					
		
			 	</section>
			 	</div>
</div>	


<jsp:include page="/WEB-INF/views/PaginaMaestra/Usuario/UserFooter.jsp"></jsp:include>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery-ui-1.9.2.custom.min.js" var="jqueryui"/>
<script src="${jqueryui}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/DT_bootstrap.js" var="dtbootstrapjs"/>
<script src="${dtbootstrapjs}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.dataTables.js" var="jquerydatablejs"/>
<script src="${jquerydatablejs}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/dynamic_table_init.js" var="dynamictablejs"/>
<script src="${dynamictablejs}"></script>
 
    <script type="text/javascript">
    	var lstDetalle = [];   
    	var total = 0.00;	
    	
    	$(".agregar").click(function(){
    		var id = $(this).attr("id");
    		var cantidad = $("#idcantidad-" + id).val();
    		var precio = $("#idPrecio-" + id).text();
    		var nombre = $("#idNombre-" + id).text();
    		
			var objLinea = {
	    			idLinea: "0",
	    			nombre: "",
	    		};
			
    		var objProducto = {
    			idProducto : id,
    			nombre: nombre,
    			precio: precio,
    			descripcion: "",
    			imagen: "",
    			objLinea: objLinea,
    		};
    		    		
    		var objDetalle = {
    				idDetalleProductoPedido : "0",
    				cantidad : cantidad,
        			objProducto: objProducto,
        	};
    		
    		lstDetalle.push(objDetalle);
    		
 
    		console.log(lstDetalle);  	
    		console.log(total);    
		
    	});
    	
    	
    	$("#addVenta").click(function(){
    		//console.log("dfgd");
    		var json = {};
    		json["lstDetalle"] = lstDetalle;  		
    		
    		$.ajax({
    			type: "POST",
    			url: "${pageContext.request.contextPath}/InsertarVenta",
    			data: JSON.stringify(json),
    			dataType: 'json',
    			processData: false,
    			contentType: 'application/json; charset=utf-8,',
    			timeout: 60000,
    			success : function (response){
    	    		window.alert("Se registr� la venta. \n Precio Total: S/ "+total);
					console.log(response);
    			},
    			error: function (e){
    				console.log(e);
    			}
    		});
    		
    		for(var i=0; i<lstDetalle.length ; i++){
    			total += lstDetalle[i].objProducto.precio * lstDetalle[i].cantidad;
    			console.log(lstDetalle[i].objProducto.precio);
    			console.log(lstDetalle[i].cantidad);
    		}  
    		window.alert("Se registr� la compra. \n Precio Total: S/ "+total);
    		
    		lstDetalle = [];  
    		total=0.0;
    		
    		console.log(total);
    	});  	
    	
    	
    	var $btns = $('.btn').click(function() {
    		  if (this.id == 'all') {
    		    $('#parent > div').fadeIn(450);
    		  } else {
    		    var $el = $('.' + this.id).fadeIn(450);
    		    $('#parent > div').not($el).hide();
    		  }
    		  $btns.removeClass('active');
    		  $(this).addClass('active');
    		})   
    	
    </script>
