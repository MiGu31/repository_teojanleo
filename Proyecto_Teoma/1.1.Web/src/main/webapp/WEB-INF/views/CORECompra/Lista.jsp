<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/DT_bootstrap.css" var="dtbootstrapcss"/>
<link href="${dtbootstrapcss}" rel="stylesheet"/>


<jsp:include page="/WEB-INF/views/PaginaMaestra/Administrador/AdmiHeader.jsp"></jsp:include>
              <!-- page start-->
              <div class="row">
                <div class="col-sm-12">
              <section class="panel">
              <header class="panel-heading">
                  Sus Compras
             <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span>
              </header>
              <div class="panel-body">
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id="dynamic-table">
              <thead>
              <tr>
                  <th>Usuario</th>              
                  <th>Fecha</th>
                  <th>Costo total(S/.)</th>
                  <th>Proveedor</th>
                  <th>Detalle</th>                  
              </tr>
              </thead>
              <tbody>  
              
              	<c:forEach items="${listaCompras}" var="compra">     
	              <tr class="gradeX">
	              	  <td>${compra.objUsuario.username}</td>	              
	                  <td>${compra.fecha}</td>
	                  <td>S/ ${compra.costoTotal}</td>
	                  <td>${compra.objProveedor.nombre}</td>                  
	                  
	                  <td align="center"> 
					  	<a class="btn btn-success" data-toggle="modal" href="#modalDetallePedido-${compra.idCompra}">
                                  Ver detalle                      
                        </a>                        
                      </td>                      
	                     <div class="modal fade modal-dialog-center " id="modalDetallePedido-${compra.idCompra}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	                         <div class="modal-dialog ">
	                             <div class="modal-content-wrap">
	                                 <div class="modal-content">
	                                     <div class="modal-header">
	                                         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                                         <h4 class="modal-title">Descripcion de la compra</h4>
	                                     </div>
	                                   <div class="modal-body row" style="text-align: center;">
	                                        <div>
										        <p class="col-md-4"><strong>Cantidad</strong></p>
										         <p class="col-md-4"><strong>Producto</strong></p>
										         <p class="col-md-4"><strong>Imagen</strong></p>
										    </div>
		                                    <c:forEach items="${compra.listaDetalle}" var="detalleCompra">    									         
								            <div>
										        <p class="col-md-4" style="margin-top: 32px;">${detalleCompra.cantidad}</p>
										        <p class="col-md-4" style="margin-top: 32px;">${detalleCompra.objProducto.nombre} </p>
										        <p class="col-md-4"><img src="${detalleCompra.objProducto.imagen}" width="100" height="100" alt=""/></p>
								             </div>	       								                  
		                                   	</c:forEach>		
			
	                                  	</div>
	                                     <div class="modal-footer">
	                                         <button data-dismiss="modal" class="btn btn-default" type="button">Cerrar</button>                                           
	                                     </div>
	                                 </div>
	                             </div>
	                         </div>
	                     </div>                      
	              </tr>  		
				</c:forEach>  

              </tfoot>
              </table>
              </div>
              </div>
              </section>
       </div>
</div>

<jsp:include page="/WEB-INF/views/PaginaMaestra/Administrador/AdmiFooter.jsp"></jsp:include>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/js/jquery-ui-1.9.2.custom.min.js" var="jqueryui"/>
<script src="${jqueryui}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/DT_bootstrap.js" var="dtbootstrapjs"/>
<script src="${dtbootstrapjs}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.dataTables.js" var="jquerydatablejs"/>
<script src="${jquerydatablejs}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/dynamic_table_init.js" var="dynamictablejs"/>
<script src="${dynamictablejs}"></script>
