<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/DT_bootstrap.css" var="dtbootstrapcss"/>
<link href="${dtbootstrapcss}" rel="stylesheet"/>


<jsp:include page="/WEB-INF/views/PaginaMaestra/Administrador/AdmiHeader.jsp"></jsp:include>
              <!-- page start-->
              <div class="row">
                <div class="col-sm-12">
              <section class="panel">
              <header class="panel-heading">
                  Promociones
             <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span>
              </header>
              <div class="panel-body">
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id="dynamic-table">
              <thead>
              <tr>
                  <th>Nombre</th>
                  <th>Fecha de creacion</th>
                  <th>Fecha de caducidad</th>
                  <th>Descripcion</th>
                  <th>Valor(%)</th>                  
                  <td><strong>Eliminar</strong></td> 
              </tr>
              </thead>
              <tbody>  
              
              	<c:forEach items="${listaPromociones}" var="promocion">     
	              <tr class="gradeX">
	                  <td>${promocion.nombre}</td>
	                  <td>${promocion.fechaCreacion}</td>
	                  <td>${promocion.fechaCaducidad}</td>
	                  <td>${promocion.descripcion}</td>
	                  <td>${promocion.valor} %</td>
	                  <frm:form method="GET" action="${pageContext.request.contextPath}/CRUDPromocion/Lista">
						<td align="center"><button name="idPromocion" value="${promocion.idPromocion} type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash-o "></i></button></td>                                							
					  </frm:form>
	              </tr>  		
				</c:forEach>  

              </tfoot>
              </table>
              </div>
              </div>
              </section>
       </div>
</div>

<jsp:include page="/WEB-INF/views/PaginaMaestra/Administrador/AdmiFooter.jsp"></jsp:include>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/js/jquery-ui-1.9.2.custom.min.js" var="jqueryui"/>
<script src="${jqueryui}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/DT_bootstrap.js" var="dtbootstrapjs"/>
<script src="${dtbootstrapjs}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.dataTables.js" var="jquerydatablejs"/>
<script src="${jquerydatablejs}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/dynamic_table_init.js" var="dynamictablejs"/>
<script src="${dynamictablejs}"></script>
