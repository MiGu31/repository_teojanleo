<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/icn/teoma-icon.ico" var="teoicn"/>
<link href="${teoicn}" rel="shortcut icon"/>

<jsp:include page="/WEB-INF/views/PaginaMaestra/Usuario/UserHeader.jsp"></jsp:include>

<jsp:include page="/WEB-INF/views/PaginaMaestra/Usuario/UserFooter.jsp"></jsp:include>
