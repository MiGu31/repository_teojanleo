<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>

<!DOCTYPE html>
<html>
<head>
<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/icn/teoma-icon.ico" var="teoicn"/>
<link href="${teoicn}" rel="shortcut icon"/>

<meta charset="encoding"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Iniciar sesion </title>
    
    <!-- Bootstrap core CSS -->    

    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/bootstrap.min.css" var="mincss"/>
    <link href="${mincss}" rel="stylesheet"/>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/bootstrap-reset.css" var="resetcss"/>
    <link href="${resetcss}" rel="stylesheet"/>   
        
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/font-awesome.css" var="fontawecss"/>
    <link href="${fontawecss}" rel="stylesheet"/>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/style-responsive.css" var="stylerespcss"/>
    <link href="${stylerespcss}" rel="stylesheet"/>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/style.css" var="stylecss"/>
    <link href="${stylecss}" rel="stylesheet"/>
    

</head>
<body class="login-body">

    <div class="container">

      <frm:form method="POST" action="VerificarAcceso" class="form-signin">
        <h2 class="form-signin-heading">INGRESAR AL SISTEMA</h2>
        <div class="login-wrap">
            <td><frm:input path="Username" class="form-control" type="text" placeholder="Username"></frm:input></td>
            <td><frm:input path="Password" class="form-control" type="password" placeholder="Contrase�a"></frm:input></td>  
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Recordarme         
            </label>
            <button class="btn btn-lg btn-login btn-block" type="submit">Entrar</button>

            <div class="registration">
                �A�n no tienes una cuenta?
                <a class="" href="${pageContext.request.contextPath}/CRUDUsuario/Insertar">
                    Registrate aqu�
                </a>
            </div>
            
            <div>
          </br>
            <%
			if(request.getParameter("msg")!=null){
				request.setCharacterEncoding("UTF-8");
 				out.print("<p>"+request.getParameter("msg")+"</p>");	
			}
			%>
          </div>
        </div>
      </frm:form>

    </div>

    <!-- js placed at the end of the document so the pages load faster --> 
	 	<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.js" var="jqueryjs"/>
    <script src="${jqueryjs}"></script>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/bootstrap.min.js" var="bootjs"/>
    <script src="${bootjs}"></script>
   

  </body>
</html>