<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<%@ page session="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/icn/teoma-icon.ico" var="teoicn"/>
<link href="${teoicn}" rel="shortcut icon"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Teoma - Bienvenido</title>

    <!-- Bootstrap Core CSS -->
    <s:url value="/resources/Bootstrap-Extranet/vendor/bootstrap/css/bootstrap.min.css" var="corecss"/>
    <link href="${corecss}" rel="stylesheet"/>

    <!-- Custom Fonts -->
    
    <s:url value="/resources/Bootstrap-Extranet/vendor/font-awesome/css/font-awesome.min.css" var="awesomecss"/>
    <link href="${awesomecss}" rel="stylesheet"/>
    
    <s:url value="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" var="font1css"/>
    <link href="${font1css}" rel="stylesheet"/>
    
    <s:url value="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic" var="font2css"/>
    <link href="${font2css}" rel="stylesheet"/>


    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <s:url value="/resources/Bootstrap-Extranet/vendor/magnific-popup/magnific-popup.css" var="magnificss"/>
    <link href="${magnificss}" rel="stylesheet"/>

    <!-- Theme CSS -->
     <s:url value="/resources/Bootstrap-Extranet/css/creative.css" var="creativecss"/>
    <link href="${creativecss}" rel="stylesheet"/>    

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">TEOMA, vive bien</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="#about">Nosotros</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#services">Caracter�sticas</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#portfolio">L�neas</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Cont�ctanos</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>
        <div class="header-content">
            <div class="header-content-inner"/>                
        </div>
    </header>

    <section class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Tenemos lo que tu salud necesita</h2>
                    <hr class="light">
                    <p class="text-faded">Contamos con los mejores productos nutricionales del mercado, utilizando la sinergia correcta de los
                    mejores insumos naturales del mundo, que juntos brindan un resultado �nico y un s�per producto para ti.</p>
                    <a href="${pageContext.request.contextPath}/CRUDUsuario/Insertar" class="page-scroll btn btn-default btn-xl sr-button">REG�STRATE</a> o
                    <a href="${pageContext.request.contextPath}/Login" class="page-scroll btn btn-default btn-xl sr-button">INICIA SESI�N</a>                    
 					
                </div>
            </div>
        </div>
    </section>

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Somos empresa</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-diamond text-primary sr-icons"></i>
                        <h3>Categor�a</h3>
                        <p class="text-muted">Somos la primera empresa peruana en ofrecer productos saludables y naturales.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-paper-plane text-primary sr-icons"></i>
                        <h3>Cambios</h3>
                        <p class="text-muted">La pol�tica de la empresa esa cambiar siempre y cuando beneficie al cliente.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-newspaper-o text-primary sr-icons"></i>
                        <h3>Actualidad</h3>
                        <p class="text-muted">Nuestros productos se elaboran con la mejor tecnolog�a.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-heart text-primary sr-icons"></i>
                        <h3>Salud</h3>
                        <p class="text-muted">Tu salud es lo mas importante para nosotros.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="no-padding" id="portfolio">
        <div class="container-fluid">
            <div class="row no-gutter popup-gallery">
             
	            <c:forEach items="${listaLineas}" var="list">         				
					<div class="col-lg-4 col-sm-6">
	                    <a href="${list.imagen}" class="portfolio-box">
	                        <img src="${list.imagen}" class="img-responsive" alt="">
	                        <div class="portfolio-box-caption">
	                            <div class="portfolio-box-caption-content">
	                                <div class="project-category text-faded">
	                                    L�nea
	                                </div>
	                                <div class="project-name">
	                                    ${list.nombre}
	                                </div>
	                            </div>
	                        </div>
	                    </a>
	                </div>			
				</c:forEach>               
                               
            </div>
        </div>
    </section>

    <aside class="bg-dark">
        <div class="container text-center">
            <div class="call-to-action">
                <h2>TU SALUD NO ES UN JUEGO!</h2>
            </div>
        </div>
    </aside>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Informaci�n adicional</h2>
                    <hr class="primary">
                    <p>Para m�s informaci�n cont�ctanos v�a telef�nica o correo electr�nico, la decisi�n es tuya!</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x sr-contact"></i>
                    <p>044-214343</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p><a href="mailto:teoma@hotmail.com">teoma@hotmail.com</a></p>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery -->
    <s:url value="/resources/Bootstrap-Extranet/vendor/jquery/jquery.min.js" var="jquerymin"/>
    <script src="${jquerymin}"></script>   
    
    <!-- Bootstrap Core JavaScript -->
    <s:url value="/resources/Bootstrap-Extranet/vendor/bootstrap/js/bootstrap.min.js" var="corejs"/>
    <script src="${corejs}"></script>   

    <!-- Plugin JavaScript   -->
    <s:url value="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" var="pluginjs"/>
    <script src="${pluginjs}"></script>   
    
    <!-- Scroll JavaScript -->    
    <s:url value="/resources/Bootstrap-Extranet/vendor/scrollreveal/scrollreveal.min.js" var="scrolljs"/>
    <script src="${scrolljs}"></script>
       
    <!-- PopUp JavaScript -->           
    <s:url value="/resources/Bootstrap-Extranet/vendor/magnific-popup/jquery.magnific-popup.min.js" var="popupjs"/>
    <script src="${popupjs}"></script>    
    
    <!-- Theme JavaScript -->
    <s:url value="/resources/Bootstrap-Extranet/js/creative.min.js" var="creativejs"/>
    <script src="${creativejs}"></script>   

</body>

</html>
