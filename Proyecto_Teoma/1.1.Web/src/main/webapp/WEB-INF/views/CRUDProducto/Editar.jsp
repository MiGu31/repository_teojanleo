<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/DT_bootstrap.css" var="dtbootstrapcss"/>
<link href="${dtbootstrapcss}" rel="stylesheet"/>


<jsp:include page="/WEB-INF/views/PaginaMaestra/Administrador/AdmiHeader.jsp"></jsp:include>


<div class="row">
	<div class="col-lg-3">
	</div>
    <div class="col-lg-6">
        <section class="panel">
            <header class="panel-heading">
                Editar Producto
            </header>
            <div class="panel-body">
                <div class=" form">
                    <frm:form class="cmxform form-horizontal tasi-form" method="POST" action="${pageContext.request.contextPath}/CRUDProducto/Editar">
                        <div class="form-group ">
                            <label for="cname" class="control-label col-lg-2">Precio</label>
                            <div class="col-lg-2">
                                <frm:input path="precio" class="form-control " minlength="4"  type="text" required="required"/>
                            </div>
                        </div>            

                        <div class="form-group ">
                            <label for="ccomment" class="control-label col-lg-2">Imagen</label>
                            <div class="col-lg-10">
                                <frm:input path="imagen" class="form-control " type="text" required="required"/>
                            </div>
                        </div>
                        
                        <frm:input style="display:none;" path="idProducto" value="${idProducto}" class="form-control " type="text" required="required"/>
                        
                        
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button align="center" type="submit" class="btn btn-primary">Editar</button>
                                <button class="btn btn-default" type="reset">Limpiar</button>
                            </div>
                        </div>

                    </frm:form>
                </div>

            </div>
        </section>
    </div>
</div>


<jsp:include page="/WEB-INF/views/PaginaMaestra/Administrador/AdmiFooter.jsp"></jsp:include>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/js/jquery-ui-1.9.2.custom.min.js" var="jqueryui"/>
<script src="${jqueryui}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/DT_bootstrap.js" var="dtbootstrapjs"/>