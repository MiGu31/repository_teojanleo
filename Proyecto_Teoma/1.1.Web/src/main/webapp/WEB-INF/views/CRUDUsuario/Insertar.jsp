<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registrarse</title>

    <!-- Bootstrap core CSS -->   
	
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/bootstrap.min.css" var="mincss"/>
    <link href="${mincss}" rel="stylesheet"/>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/bootstrap-reset.css" var="resetcss"/>
    <link href="${resetcss}" rel="stylesheet"/>   
        
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/font-awesome.css" var="fontawecss"/>
    <link href="${fontawecss}" rel="stylesheet"/>

    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/jquery.steps.css" var="stepcss"/>
    <link href="${stepcss}" rel="stylesheet"/>    

    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/style.css" var="stylecss"/>
    <link href="${stylecss}" rel="stylesheet"/>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/style-responsive.css" var="stylerespcss"/>
    <link href="${stylerespcss}" rel="stylesheet"/>    
 
    
</head>
<body>

</br>
<br>
<br>
</br>
<br>
<br>

	<h2 class="modal-title" align="center">REGISTRO DE USUARIO</h2>

     <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row" >
                  <div class="col-lg-10">
                      <section class="panel">
                       
                          <div class="panel-body">
                              <div class="stepy-tab">
                                  <ul id="default-titles" class="stepy-titles clearfix">
                                      <li id="default-title-0" class="current-step">
                                          <div>Paso 1</div>
                                      </li>
                                      <li id="default-title-1" class="">
                                          <div>Paso 2</div>
                                      </li>
                                      <li id="default-title-2" class="">
                                          <div>Paso 3</div>
                                      </li>
                                  </ul>
                              </div>
                              
                            <frm:form method="POST" class="form-horizontal" id="default" action="${pageContext.request.contextPath}/CRUDUsuario/Insertar">
                                  <fieldset title="Paso 1" class="step" id="default-step-0">
                                      <legend> </legend>
                                      <div class="form-group">
	                                      <label class="col-lg-2 control-label">Usuario</label>
	                                      <div class="col-lg-10">
	                                          <frm:input path="objUsuario.username" type="text" id="idUser" class="form-control" placeholder="Usuario" required="required"/>
	                                      </div>
                                      </div>
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">Contraseña</label>
                                          <div class="col-lg-10">
                                              <frm:input path="objUsuario.password" type="password" id="idPassword" class="form-control" placeholder="Contraseña" required="required"/>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">Correo electrónico</label>
                                          <div class="col-lg-10">
                                              <frm:input path="objUsuario.email" type="text" id="idEmail" class="form-control" placeholder="Correo electrónico" required="required"/>
                                          </div>
                                      </div>
                           

                                  </fieldset>
                                  <fieldset title="Paso 2" class="step" id="default-step-1" >
                                      <legend> </legend>
                                      
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">DNI</label>
                                          <div class="col-lg-10">
                                               <frm:input path="dni" type="text" id="idDNI" class="form-control" placeholder="DNI" required="required"/>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">Nombres</label>
                                          <div class="col-lg-10">
                                               <frm:input path="nombres" type="text" id="idNombres"  class="form-control" placeholder="Nombres" required="required"/>
                                          </div>
                                      </div>
    									<div class="form-group">
                                          <label class="col-lg-2 control-label">Apellidos</label>
                                          <div class="col-lg-10">
                                               <frm:input path="apellidos" type="text" id="idApellidos" class="form-control" placeholder="Apellidos" required="required"/>
                                          </div>
                                      </div>
                                      
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">Telefono Celular</label>
                                          <div class="col-lg-10">
                                               <frm:input path="telefonoCelular" type="text" id="idCelular" class="form-control" placeholder="Telefono Celular" required="required"/>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">Telefono Fijo</label>
                                          <div class="col-lg-10">
                                               <frm:input path="telefonoFijo" type="text" id="idFijo" class="form-control" placeholder="Telefono Fijo" required="required"/>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">Direccion</label>
                                          <div class="col-lg-10">
                                               <frm:input path="direccion" type="text" id="idDireccion" class="form-control" placeholder="Direccion" required="required"/>
                                          </div>
                                      </div>

                                  </fieldset>
                                  
                                  
                                  <fieldset title="Paso 3" class="step" id="default-step-2" >
                                      <legend> </legend>
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">Usuario</label>
                                          <div class="col-lg-10">
                                              <p class="form-control-static" id="outUser"> </p>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">Contraseña</label>
                                          <div class="col-lg-10">
                                              <p class="form-control-static" id="outPass"></p>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">Correo electrónico</label>
                                          <div class="col-lg-10">
                                              <p class="form-control-static" id="outEmail"></p>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">DNI</label>
                                          <div class="col-lg-10">
                                              <p class="form-control-static" id="outDNI"></p>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">Nombres</label>
                                          <div class="col-lg-10">
                                              <p class="form-control-static" id="outNombres"></p>
                                          </div>
                                      </div>
                                      
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">Apellidos</label>
                                          <div class="col-lg-10">
                                              <p class="form-control-static" id="outApellidos"></p>
                                          </div>
                                      </div>
                                      
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">Telefono Celular</label>
                                          <div class="col-lg-10">
                                              <p class="form-control-static" id="outCelular"></p>
                                          </div>
                                      </div>
                                      
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">Telefono Fijo</label>
                                          <div class="col-lg-10">
                                              <p class="form-control-static" id="outFijo"></p>
                                          </div>
                                      </div>
                                      
                                      <div class="form-group">
                                          <label class="col-lg-2 control-label">Dirección</label>
                                          <div class="col-lg-10">
                                              <p class="form-control-static" id="outDireccion"></p>
                                          </div>
                                      </div>
 
                                  </fieldset>
                                  <input type="submit" class="finish btn btn-success" value="Registrar"/>
                              </frm:form>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>

  	<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.js" var="jqueryjs"/>
    <script src="${jqueryjs}"></script>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/bootstrap.min.js" var="bootjs"/>
    <script src="${bootjs}"></script>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.dcjqaccordion.2.7.js" var="accordionjs"/>
    <script class="include" src="${accordionjs}"></script>      
        
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.scrollTo.min.js" var="jquerynicescrolljs"/>
    <script src="${jquerynicescrolljs}"></script>   
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.nicescroll.js" var="jquerynicejs"/>
    <script src="${jquerynicejs}"></script>    
                           
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/respond.min.js" var="respondjs"/>
    <script src="${respondjs}"></script>      
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/slidebars.min.js" var="slidebarsjs"/>
    <script src="${slidebarsjs}"></script>       
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/bootstrap-validator.min.js" var="validatorjs"/>
    <script src="${validatorjs}"></script>        

    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/common-scripts.js" var="commonjs"/>
    <script src="${commonjs}"></script>  

    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.steps.min.js" var="jquerystepsjs"/>
    <script src="${jquerystepsjs}"></script>     
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.stepy.js" var="jquerystepyjs"/>
    <script src="${jquerystepyjs}"></script>         
        
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.validate.min.js" var="jqueryvalijs"/>
    <script src="${jqueryvalijs}"></script> 

 
  	<script>

      //step wizard

      $(function() {
          $('#default').stepy({
              backLabel: 'Regresar',
              block: true,
              nextLabel: 'Siguiente',
              titleClick: true,
              titleTarget: '.stepy-tab'
          });
      });

  </script>
  
  <script type="text/javascript">
  
	    
	  $("#idUser").change(function() {
		    $("#outUser").text($(this).val());
		});
	  	  
	  $('#idPassword').change(function() {
		    $('#outPass').text($(this).val());
		});
	  
	  $('#idEmail').change(function() {
		    $('#outEmail').text($(this).val());
		});
	  
	  $('#idDNI').change(function() {
		    $('#outDNI').text($(this).val());
		});
	  
	  $('#idNombres').change(function() {
		    $('#outNombres').text($(this).val());
		});
	  
	  $('#idApellidos').change(function() {
		    $('#outApellidos').text($(this).val());
		});
	  
	  $('#idCelular').change(function() {
		    $('#outCelular').text($(this).val());
		});
	  
	  $('#idFijo').change(function() {
		    $('#outFijo').text($(this).val());
		});
	  
	  $('#idDireccion').change(function() {
		    $('#outDireccion').text($(this).val());
		});
  </script>

  </body>
</html>