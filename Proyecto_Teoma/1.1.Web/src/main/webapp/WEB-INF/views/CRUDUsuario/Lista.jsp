<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/DT_bootstrap.css" var="dtbootstrapcss"/>
<link href="${dtbootstrapcss}" rel="stylesheet"/>

<jsp:include page="/WEB-INF/views/PaginaMaestra/Administrador/AdmiHeader.jsp"></jsp:include>
              <!-- page start-->
	     <div class="row">
	      <div class="col-sm-12">
	     <section class="panel">
	     <header class="panel-heading">
	         Clientes habilitados
	    <span class="tools pull-right">
	       <a href="javascript:;" class="fa fa-chevron-down"></a>
	       <a href="javascript:;" class="fa fa-times"></a>
	    </span>
	     </header>
	     <div class="panel-body">
	     <div class="adv-table">
	     <table  class="display table table-bordered table-striped" id="dynamic-table">
	      <thead>
	       <tr>
	           <th>Username</th>
	           <th>DNI</th>
	           <th>Correo electrónico</th>
	           <th>Telefono</th>
	           <th>Direccion</th>
	           <th><i class=" fa fa-edit"></i>Estado</th>	
	           <td align="center">Cambiar</td>           
	       </tr>
	      </thead>
	      <tbody>            
	   		<c:forEach items="${listaClientes}" var="cliente">  
	   			<c:if test = "${cliente.objUsuario.estado == true}">
	   				<tr class="gradeX">
			          <td>${cliente.objUsuario.username}</td>
			          <td>${cliente.dni}</td>
			          <td>${cliente.objUsuario.email}</td>
		              <td>${cliente.telefonoCelular}</td>
		              <td>${cliente.direccion}</td>	
		              <td>Habilitado</td>	
		              <frm:form method="GET" action="${pageContext.request.contextPath}/CRUDUsuario/Lista">
							<td align="center"><button name="idUsuario" value="${cliente.objUsuario.idUsuario} type="submit" class="btn btn-danger btn-sm"><i class="fa fa-lock"></i></button></td>                                							
					  </frm:form>    
		     	 	</tr> 			      
			    </c:if>      	
			</c:forEach>  	
	     </tfoot>
	     </table>
	     </div>
	     </div>
	     </section>
	     
	     
	      <section class="panel">
	     <header class="panel-heading">
	         Clientes inhabilitados
	    <span class="tools pull-right">
	       <a href="javascript:;" class="fa fa-chevron-down"></a>
	       <a href="javascript:;" class="fa fa-times"></a>
	    </span>
	     </header>
	     <div class="panel-body">
	     <div class="adv-table">
	     <table  class="display table table-bordered table-striped" id="dynamic-table">
	      <thead>
	       <tr>
	           <th>Username</th>
	           <th>DNI</th>
	           <th>Correo electrónico</th>
	           <th>Telefono</th>
	           <th>Direccion</th>
	           <th><i class=" fa fa-edit"></i>Estado</th>	
	           <td align="center">Cambiar</td>	                                                                                             	           
	       </tr>
	      </thead>
	      <tbody>            
	   		<c:forEach items="${listaClientes}" var="cliente">  
	   			<c:if test = "${cliente.objUsuario.estado == false}">
	   				<tr class="gradeX">
			          <td>${cliente.objUsuario.username}</td>
			          <td>${cliente.dni}</td>
			          <td>${cliente.objUsuario.email}</td>
		              <td>${cliente.telefonoCelular}</td>
		              <td>${cliente.direccion}</td>	
		              <td>Inhabilitado</td>	 
		              <frm:form method="GET" action="${pageContext.request.contextPath}/CRUDUsuario/Lista">
		              	 <td align="center"><button name="idUsuario" value="${cliente.objUsuario.idUsuario} type="submit" class="btn btn-success btn-sm"><i class="fa fa-unlock"></i></button></td>                                         	
					  </frm:form>  
		     	 	</tr> 			      
			    </c:if>      	
			</c:forEach>  	
	     </tfoot>
	     </table>
	     </div>
	     </div>
	     </section>
	     
	</div>
</div>

<jsp:include page="/WEB-INF/views/PaginaMaestra/Administrador/AdmiFooter.jsp"></jsp:include>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/js/jquery-ui-1.9.2.custom.min.js" var="jqueryui"/>
<script src="${jqueryui}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/DT_bootstrap.js" var="dtbootstrapjs"/>
<script src="${dtbootstrapjs}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.dataTables.js" var="jquerydatablejs"/>
<script src="${jquerydatablejs}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/dynamic_table_init.js" var="dynamictablejs"/>
<script src="${dynamictablejs}"></script>

