<%@page import="com.presentacion.pojo.entUsuario"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Descriptor de sevicios</title>

    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/bootstrap.min.css" var="mincss"/>
    <link href="${mincss}" rel="stylesheet"/>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/bootstrap-reset.css" var="resetcss"/>
    <link href="${resetcss}" rel="stylesheet"/>   
        
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/font-awesome.css" var="fontawecss"/>
    <link href="${fontawecss}" rel="stylesheet"/>

    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/jquery.steps.css" var="stepcss"/>
    <link href="${stepcss}" rel="stylesheet"/>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/slidebars.css" var="slidebarscss"/>
    <link href="${slidebarscss}" rel="stylesheet"/>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/style-responsive.css" var="stylerespcss"/>
    <link href="${stylerespcss}" rel="stylesheet"/>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/style.css" var="stylecss"/>
    <link href="${stylecss}" rel="stylesheet"/>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/jquery.easy-pie-chart.css" var="jquerychart"/>
    <link href="${jquerychart}" rel="stylesheet" media="screen"/>      
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/owl.carousel.css" var="carousel"/>
    <link href="${carousel}" rel="stylesheet"/> 

</head>
<body>
        <div class="row">
        
          <div class="col-lg-3">
          </div>
<div align="center">
	<h1>DESCRIPCION DE SERVICIOS - TEOMA</h1>
</div>
<br>
<br>
<br>
          <div class="col-lg-3">
          </div>
              <div class="col-lg-6">
              
                      <div id="accordion" class="panel-group m-bot20">
                      
                      	   		<c:forEach items="${listaDescripcion}" var="servicio">  
                      	   		
                      	   		    <div class="panel panel-default">
			                              <div class="panel-heading">
			                                  <h4 class="panel-title">                                  
			                                      <a href="#${servicio.key}" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
			                                          ${servicio.key}
			                                      </a>
			                                  </h4>
			                              </div>			                              
			                              
			                              <div class="panel-collapse collapse in" id="${servicio.key}">
			                                  <div class="panel-body">
			                                  		${servicio.value}
			                                  </div>
			                              </div>
			                   </c:forEach>            
			                              
			          </div>
    	
						

                      </div>
                  </div>
		</div>



</body>

   	<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.js" var="jqueryjs"/>
    <script src="${jqueryjs}"></script>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/bootstrap.min.js" var="bootjs"/>
    <script src="${bootjs}"></script>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.dcjqaccordion.2.7.js" var="accordionjs"/>
    <script class="include" src="${accordionjs}"></script>      
        
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.scrollTo.min.js" var="jquerynicescrolljs"/>
    <script src="${jquerynicescrolljs}"></script>   
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.nicescroll.js" var="jquerynicejs"/>
    <script src="${jquerynicejs}"></script>
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.sparkline.js" var="jquertsparkjs"/>
    <script src="${jquertsparkjs}"></script>        
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.easy-pie-chart.js" var="jquerypiejs"/>
    <script src="${jquerypiejs}"></script>                           
                       
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/owl.carousel.js" var="owcarejs"/>
    <script src="${owcarejs}"></script>       
        
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.customSelect.min.js" var="customjs"/>
    <script src="${customjs}"></script>   
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/respond.min.js" var="respondjs"/>
    <script src="${respondjs}"></script>       
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/bootstrap-validator.min.js" var="validatorjs"/>
    <script src="${validatorjs}"></script>        

    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/common-scripts.js" var="commonjs"/>
    <script src="${commonjs}"></script>  

    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.steps.min.js" var="jquerystepsjs"/>
    <script src="${jquerystepsjs}"></script>     
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.stepy.js" var="jquerystepyjs"/>
    <script src="${jquerystepyjs}"></script>         
        
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery.validate.min.js" var="jqueryvalijs"/>
    <script src="${jqueryvalijs}"></script> 

    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/slidebars.min.js" var="slidejs"/>
    <script src="${slidejs}"></script> 
    
    <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/spinner.min.js" var="spinnerjs"/>
    <script src="${spinnerjs}"></script>  

   <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/sparkline-chart.js" var="sparkchartjs"/>
   <script src="${sparkchartjs}"></script>
   
   <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/easy-pie-chart.js" var="easychartjs"/>
   <script src="${easychartjs}"></script>
   
   <s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/count.js" var="countjs"/>
   <script src="${countjs}"></script>


 <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

      $(window).on("resize",function(){
          var owl = $("#owl-demo").data("owlCarousel");
          owl.reinit();
      });

  </script>
</html>