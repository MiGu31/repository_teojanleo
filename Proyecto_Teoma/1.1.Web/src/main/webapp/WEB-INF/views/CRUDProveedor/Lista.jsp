<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
 

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/DT_bootstrap.css" var="dtbootstrapcss"/>
<link href="${dtbootstrapcss}" rel="stylesheet"/>


<jsp:include page="/WEB-INF/views/PaginaMaestra/Administrador/AdmiHeader.jsp"></jsp:include>


<div class="row">
	      <div class="col-sm-12">
	     <section class="panel">
	     <header class="panel-heading">
	         Proveedores
	    <span class="tools pull-right">
	       <a href="javascript:;" class="fa fa-chevron-down"></a>
	       <a href="javascript:;" class="fa fa-times"></a>
	    </span>
	     </header>
	     <div class="panel-body">
	     <div class="adv-table">
	     <table  class="display table table-bordered table-striped" id="dynamic-table">
	      <thead>
	       <tr>
	           <th>Nombre</th>
	           <th>Direcci�n</th>
	           <th>Celular</th>
	           <th>Correo</th>
               <td align="center"><strong>Productos</strong></td>
	       </tr>
	      </thead>
	      <tbody>            
	   		<c:forEach items="${listaProveedores}" var="objProveedor">  
	   				<tr class="gradeX">
			          <td>${objProveedor.nombre}</td>
			          <td>${objProveedor.direccion}</td>
			          <td>${objProveedor.celular}</td>
		              <td>${objProveedor.correo}</td>
		              <frm:form method="GET" action="${pageContext.request.contextPath}/CRUDProveedor/ListaProductos">
							<td align="center"><button name="idProveedor" value="${objProveedor.idProveedor} type="submit" class="btn btn-success">Ver productos</button></td>                                							
					  </frm:form>                    
		     	 	</tr> 			        	
			</c:forEach>  	
	     </tfoot>
	     </table>
	     </div>
	     </div>
</section>


<jsp:include page="/WEB-INF/views/PaginaMaestra/Administrador/AdmiFooter.jsp"></jsp:include>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/js/jquery-ui-1.9.2.custom.min.js" var="jqueryui"/>
<script src="${jqueryui}"></script>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/DT_bootstrap.js" var="dtbootstrapjs"/>