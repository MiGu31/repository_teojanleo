<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
 

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/css/DT_bootstrap.css" var="dtbootstrapcss"/>
<link href="${dtbootstrapcss}" rel="stylesheet"/>

<jsp:include page="/WEB-INF/views/PaginaMaestra/Administrador/AdmiHeader.jsp"></jsp:include>

<s:url value="/resources/Bootstrap-Intranet/Bootstrap-Admin/assets/select2/css/select2.min.css" var="select2css"/>
<link href="${select2css}" rel="stylesheet"/>

<div class="row">
	<div class="col-lg-6">
		<section class="panel">
			<header class="panel-heading">
				Productos del proveedor <span class="tools pull-right"> <a
					href="javascript:;" class="fa fa-chevron-down"></a> <a
					href="javascript:;" class="fa fa-times"></a>
				</span>
			</header>

			<div class="panel-body">
				<div class="adv-table">
					<table class="display table table-bordered table-striped"
						id="dynamic-table">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Imagen</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listaProductos}" var="objProducto">
								<tr class="gradeX">
									<td>${objProducto.nombre}</td>
									<td><img src="${objProducto.imagen}" width="100"
										height="100" alt=""></td>
								</tr>
							</c:forEach>
						</tfoot>
					</table>
				</div>
			</div>


		</section>
	</div>
	
		<div class="col-lg-6">
		<section class="panel">
			<header class="panel-heading">
				Agregar productos <span class="tools pull-right"> <a
					href="javascript:;" class="fa fa-chevron-down"></a> <a
					href="javascript:;" class="fa fa-times"></a>
				</span>
			</header>
			<div class="panel-body">

				
					<div class="form-group">
					   <label class="">Seleccione </label>
					   					
						<select class="js-example-basic-multiple" multiple="multiple" id="bofi">
							<c:forEach items="${listaProductosAdd}" var="objProductos">
								<option value="${objProductos.idProducto}">${objProductos.nombre}</option>
							</c:forEach>
						</select>

					</div>
					
					<div align="right">
						<button id="cleanSelect" class="btn btn-default" type="button">Limpiar</button>
						<!--  <button id="guardarSelect" class="btn btn-success">Guardar</button>-->
						<a id="guardarSelect" class="btn btn-success" href="${pageContext.request.contextPath}/CRUDProveedor/Lista"> Guardar</a>
					</div>						

			</div>
		</section>
	</div>
	
	
</div>

</section>


<jsp:include
	page="/WEB-INF/views/PaginaMaestra/Administrador/AdmiFooter.jsp"></jsp:include>
<!--<s:url
	value="/resources/Bootstrap-Intranet/Bootstrap-Admin/js/jquery-ui-1.9.2.custom.min.js"
	var="jqueryui" />
<script src="${jqueryui}"></script>-->


<s:url
	value="/resources/Bootstrap-Intranet/Bootstrap-Admin/assets/select2/js/select2.min.js"
	var="select2js" />
<script src="${select2js}"></script>


<script >
	$(document).ready(function() {
		console.log("hola");
		$(".js-example-basic-multiple").select2();
		
	});
	
	$( "#cleanSelect" ).click(function() {
		$("#bofi").val('').trigger('change')
		});
	
	//Enviar a controlador
	$("#guardarSelect").click(function(){
		var json = {};
		var listaProductos = $("#bofi").select2('val');
		listaProductos=listaProductos.map(Number);
		console.log(listaProductos);		
		
		json["lstProductos"] = listaProductos;  		

		
		$.ajax({
			type: "POST",
			url: "${pageContext.request.contextPath}/InsertarProductos",
			data: JSON.stringify(json),
			dataType: 'json',
			processData: false,
			contentType: 'application/json; charset=utf-8,',
			timeout: 60000,
			success : function (response){
	    		window.alert("Funcion� AJAX");
				console.log(response);  			   
			},
			error: function (e){
				console.log(e);
			}
		});
		
		console.log(json);

		
	});  
	
	
</script>




