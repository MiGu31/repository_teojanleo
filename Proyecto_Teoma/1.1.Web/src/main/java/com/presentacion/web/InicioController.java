package com.presentacion.web;

import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.presentacion.pojo.entLinea;
import com.presentacion.pojo.entReclamo;
import com.presentacion.pojo.entUsuario;

@Controller
public class InicioController {
	
	ResourceBundle resourceBundle = ResourceBundle.getBundle("route");	


	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home() {
	
		try {
			RestTemplate rest = new RestTemplate();			
			//String URL = "http://server-service.azurewebsites.net/Restful/Linea/ListarLineas";
			String URL = resourceBundle.getString("URLService")+"Linea/ListarLineas";
			ArrayList<entLinea> listaLineas = new ArrayList<entLinea>();		
							
			ArrayList<entLinea> listaService = rest.getForObject(URL, listaLineas.getClass()); 		
			
			ModelAndView m = new ModelAndView("Home/Inicio");
			m.addObject("listaLineas",listaService);
					
			return m;		
			
		}
		catch (Exception e) {
			return new ModelAndView("frmError","error",e.getMessage());
		}
	}
		
	@RequestMapping(value = "/Login", method = RequestMethod.GET)
    public ModelAndView Login(HttpServletRequest request,HttpServletResponse response) throws Exception {
        ModelAndView login = new ModelAndView("Home/Login","command", new entUsuario());
        return login;     
    }	
	
	@RequestMapping(value = "/Home/IntranetAdministrador", method = RequestMethod.GET)
    public ModelAndView IntranetAdministrador(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws Exception {
        ModelAndView intranetAdm = new ModelAndView("/Home/IntranetAdministrador");
        return intranetAdm;     
    }		
	
	
	@RequestMapping(value = "/Home/IntranetUsuario", method = RequestMethod.GET)
    public ModelAndView IntranetUsuario(HttpServletRequest request,HttpServletResponse response, HttpSession session) throws Exception {
        ModelAndView intranetUser = new ModelAndView("/Home/IntranetUsuario");
        return intranetUser;     
    }	
	
	
	
	
	
}
