package com.presentacion.web;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.presentacion.pojo.entDetalleProductoPedido;
import com.presentacion.pojo.entLinea;
import com.presentacion.pojo.entPedido;
import com.presentacion.pojo.entPersona;
import com.presentacion.pojo.entProducto;
import com.presentacion.pojo.entUsuario;

@Controller
public class PedidoController {	

	ResourceBundle resourceBundle = ResourceBundle.getBundle("route");	
	
	@RequestMapping(value = "/InsertarVenta", method = RequestMethod.POST)
	@ResponseBody
	public String InsertarVentaPost(@RequestBody String json, Model model, HttpSession session)throws Exception {
	
		try {
			entUsuario objUsuarioSesion = (entUsuario) session.getAttribute("objUsuario");
			
			RestTemplate rest = new RestTemplate();
			
			ObjectMapper om = new ObjectMapper();			
			JsonNode jsonNode = om.readTree(json) ; 		
			
			ArrayList<entDetalleProductoPedido> lstDetalle =  om.convertValue(jsonNode.get("lstDetalle"),
					new TypeReference<ArrayList<entDetalleProductoPedido>>() {});	
						
			entPedido objPedido = new entPedido();
			entUsuario objUsuario = new entUsuario();
			//objUsuario.setIdUsuario(objUsuarioSesion);
			objPedido.setObjUsuario(objUsuarioSesion);
			objPedido.setListaDetalle(lstDetalle);
			
			String url = resourceBundle.getString("URLService")+"Pedido/InsertarPedido";

			ResponseEntity<entPedido> result = rest.postForEntity(url, objPedido, entPedido.class);	
			
			ModelAndView m = new ModelAndView("COREPedido/Insertar");				
					
			return "COREPedido/Insertar";		
			
		}
		catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	
	
	@RequestMapping(value = "/COREPedido/Insertar", method = RequestMethod.GET)
	public ModelAndView InsertarVentaGET() {
	
		try {
			RestTemplate rest = new RestTemplate();			
			
			String URL = resourceBundle.getString("URLService")+"Producto/ListarProductos";
			String URLinea = resourceBundle.getString("URLService")+"Linea/ListarLineas";

			ArrayList<entProducto> listaProductos = new ArrayList<entProducto>();	
			ArrayList<entLinea> listaLineas = new ArrayList<entLinea>();	
			
			ArrayList<entLinea> listaServiceLineas = rest.getForObject(URLinea, listaLineas.getClass()); 							
			ArrayList<entProducto> listaServiceProductos = rest.getForObject(URL, listaProductos.getClass()); 
			
			ModelAndView m = new ModelAndView("/COREPedido/Insertar");
			m.addObject("listaProductos",listaServiceProductos);													
			m.addObject("listaLineas",listaServiceLineas);	
			
			return m;		
			
		}
		catch (Exception e) {
			return new ModelAndView("frmError","error",e.getMessage());
		}
	}
	
	@RequestMapping(value = "/COREPedido/ListaUsuario", method = RequestMethod.GET)
	public ModelAndView home(Locale locale, Model model,HttpSession session) {
	
		try {
			ObjectMapper mapper = new ObjectMapper();		

			entUsuario objUsuarioSesion = (entUsuario) session.getAttribute("objUsuario");			
			RestTemplate rest = new RestTemplate();
			
			String URLPedidos = resourceBundle.getString("URLService")+"Pedido/ListarPedidos?idUsuario="+objUsuarioSesion.getIdUsuario();
			ArrayList<entPedido> listaPedidos = mapper.readValue(new URL(URLPedidos),mapper.getTypeFactory().constructCollectionType(ArrayList.class, entPedido.class));									
										
			String URLDetalleProPedidos = resourceBundle.getString("URLService")+"DetalleProductoPedidoController/Listar";			
			ArrayList<entDetalleProductoPedido> listaDetalleProPedidos = mapper.readValue(new URL(URLDetalleProPedidos),mapper.getTypeFactory().constructCollectionType(ArrayList.class, entDetalleProductoPedido.class)); 								

			for(entPedido p : listaPedidos)
			{	
				ArrayList<entDetalleProductoPedido> listaDetallePedidoTemporal = new ArrayList<entDetalleProductoPedido>();
				for(entDetalleProductoPedido epp : listaDetalleProPedidos)
				{
					if(p.getIdPedido()==epp.getObjPedido().getIdPedido()) {
						listaDetallePedidoTemporal.add(epp);
					}
				}

				p.setListaDetalle(listaDetallePedidoTemporal);
				
			}	
			
			ModelAndView m = new ModelAndView("COREPedido/ListaUsuario");
			m.addObject("listaPedidos",listaPedidos);
					
			return m;		
			
		}
		catch (Exception e) {
			return new ModelAndView("frmError","error",e.getMessage());
		}
	}	
	
	
	@RequestMapping(value = "/COREPedido/ListaTotal", method = RequestMethod.GET)
	public ModelAndView ListaPedidosTotal(Locale locale, Model model)throws Exception {
	
		try {
			
			ObjectMapper mapper = new ObjectMapper();		

			String URLPedidos = resourceBundle.getString("URLService")+"Pedido/ListarPedidosGenerales";			
			ArrayList<entPedido> listaPedidos = mapper.readValue(new URL(URLPedidos),mapper.getTypeFactory().constructCollectionType(ArrayList.class, entPedido.class));							
												
			String URLDetalleProPedidos = resourceBundle.getString("URLService")+"DetalleProductoPedidoController/Listar";			
			ArrayList<entDetalleProductoPedido> listaDetalleProPedidos = mapper.readValue(new URL(URLDetalleProPedidos),mapper.getTypeFactory().constructCollectionType(ArrayList.class, entDetalleProductoPedido.class)); 								
			
			for(entPedido p : listaPedidos)
			{	
				ArrayList<entDetalleProductoPedido> listaDetallePedidoTemporal = new ArrayList<entDetalleProductoPedido>();
				for(entDetalleProductoPedido epp : listaDetalleProPedidos)
				{
					if(p.getIdPedido()==epp.getObjPedido().getIdPedido()) {
						listaDetallePedidoTemporal.add(epp);
					}
				}
				System.out.println(p.getFecha());
				p.setListaDetalle(listaDetallePedidoTemporal);
			}	
			
			
			ModelAndView m = new ModelAndView("COREPedido/ListaTotal");
			m.addObject("listaPedidos",listaPedidos);
					
			return m;		
			
		}
		catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("frmError","error",e.getMessage());
		}
	}

}
