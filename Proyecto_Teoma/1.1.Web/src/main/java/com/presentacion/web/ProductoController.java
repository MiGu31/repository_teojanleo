package com.presentacion.web;

import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.presentacion.pojo.entLinea;
import com.presentacion.pojo.entPersona;
import com.presentacion.pojo.entProducto;
import com.presentacion.pojo.entPromocion;
import com.presentacion.pojo.entReclamo;

@Controller
public class ProductoController {
	
	ResourceBundle resourceBundle = ResourceBundle.getBundle("route");	

	
	@RequestMapping(value = "/CRUDProducto/Lista", method = RequestMethod.GET)
    public ModelAndView ListarProductos(HttpServletRequest req,HttpServletResponse response) throws Exception {
		
		try {
			RestTemplate rest = new RestTemplate();			
			
			if(req.getParameter("idProducto")!=null) {
				String idString = req.getParameter("idProducto");						
				int idInt = new Integer(idString.substring(0, idString.indexOf(" "))); 
				
				String URLi = resourceBundle.getString("URLService")+"Producto/Eliminar?idProducto="+idInt;
				Boolean elimino = rest.getForObject(URLi, Boolean.class);

			}	
									
			String URL = resourceBundle.getString("URLService")+"Producto/ListarProductos";
			ArrayList<entProducto> listaProductos = new ArrayList<entProducto>();		
							
			ArrayList<entProducto> listaService = rest.getForObject(URL, listaProductos.getClass()); 		
			
			ModelAndView m = new ModelAndView("/CRUDProducto/Lista");
			m.addObject("listaProductos",listaService);
					
			return m;				
			
		}
		catch (Exception e) {
			return new ModelAndView("frmError","error",e.getMessage());
		}
         
    }	
	
	
	
	@RequestMapping(value = "/CRUDProducto/Insertar", method = RequestMethod.GET)
	public ModelAndView InsertarProductoGET() {		
		try {
			RestTemplate restLinea = new RestTemplate();
			
			String URLlineas=resourceBundle.getString("URLService")+"Linea/ListarLineas";
			ArrayList<entLinea> listaLineas = new ArrayList<entLinea>();
			ArrayList<entLinea> resultLineas = restLinea.getForObject(URLlineas, listaLineas.getClass());
			
			ModelAndView m = new ModelAndView("/CRUDProducto/Insertar", "command", new entProducto());
			m.addObject("listaLineas",resultLineas);			
			return m;

		} catch (Exception e) {
			return new ModelAndView("frmError", "error", e.getMessage());
		}	
	}
	
	@RequestMapping(value = "/CRUDProducto/Insertar", method = RequestMethod.POST)
	public String InsertarProductoPOST(@ModelAttribute("SpringWeb") entProducto p, @RequestParam("idLinea") String idLinea) {		
		try {		
			if(Integer.parseInt(idLinea)!=0)
				{
					entLinea linea = new entLinea();
					linea.setIdLinea(Integer.parseInt(idLinea));
					p.setObjLinea(linea);
					
					RestTemplate restLinea = new RestTemplate();
					RestTemplate restProducto = new RestTemplate();
					
					String URLlineas=resourceBundle.getString("URLService")+"Linea/ListarLineas";
					ArrayList<entLinea> listaLineas = new ArrayList<entLinea>();
					ArrayList<entLinea> resultLineas = restLinea.getForObject(URLlineas, listaLineas.getClass());		
			
			
					String URLProducto = resourceBundle.getString("URLService")+"Producto/InsertarProducto";			
					ResponseEntity<entProducto> resultProducto = restProducto.postForEntity(URLProducto, p, entProducto.class);	
					
					if(resultProducto!=null){
						return "redirect:/Home/IntranetAdministrador";
					}
					else
					{
						return "redirect:/Home/IntranetAdministrador";	
					}		
				}
			else
			{
				return "redirect:/CRUDProducto/Insertar?msg=Por favor, seleccione una linea valida.";
			}

			
		} catch (Exception e) {
			e.printStackTrace();
			return "Error";
		}	
	}
	
	@RequestMapping(value = "/CRUDProducto/Editar", method = RequestMethod.GET)
	public ModelAndView EditarProducto(HttpServletRequest req) {		
		try {
				
			req.getParameter("idProducto");
			String idString = req.getParameter("idProducto");						
			int idInt = new Integer(idString.substring(0, idString.indexOf(" "))); 
				

			ModelAndView m = new ModelAndView("/CRUDProducto/Editar", "command", new entProducto());
			m.addObject("idProducto", idInt);
			
			return m;
			
		} catch (Exception e) {
			return new ModelAndView("frmError", "error", e.getMessage());
		}	
	}	

	@RequestMapping(value = "/CRUDProducto/Editar", method = RequestMethod.POST)
	public String EditarProductoPOST(@ModelAttribute("SpringWeb") entProducto p, HttpServletResponse response) {		
		try {
				
			RestTemplate restProducto = new RestTemplate();

			String URLProducto = resourceBundle.getString("URLService")+"Producto/EditarProducto";			
			ResponseEntity<entProducto> resultProducto = restProducto.postForEntity(URLProducto, p, entProducto.class);
			
			return "redirect:/CRUDProducto/Lista";
			
			//return new ModelAndView("/CRUDProducto/Editar", "command", new entProducto());
		} catch (Exception e) {
			e.printStackTrace();
			return "Error";
		}	
	}	
}
