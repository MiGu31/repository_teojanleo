package com.presentacion.web;

import java.util.ResourceBundle;

import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import com.presentacion.pojo.entUsuario;

@Controller
public class UsuarioController {
	
	ResourceBundle resourceBundle = ResourceBundle.getBundle("route");	
		
	@RequestMapping(value = "/VerificarAcceso", method = RequestMethod.POST)
	public String VerificarAcceso(@ModelAttribute("SpringWeb")entUsuario u, HttpSession session){	
		try {
			String Username = u.getUsername();
			String Password = u.getPassword();
			
			RestTemplate rest = new RestTemplate();
			//String URL = resourceBundle.getString("URLService")+"Usuario/VerificarAcceso"+"?Username="+Username+"&Password="+Password;
			String URL = resourceBundle.getString("URLService")+"Usuario/VerificarAcceso"+"?Username="+Username+"&Password="+Password;
			entUsuario usuarioService = rest.getForObject(URL, u.getClass());			
						
			if(usuarioService==null) return "redirect:/Login?msg=Usuario o password incorrecto";

			session.setAttribute("objUsuario", usuarioService);
			String tipoUsuario = usuarioService.getTipoUsuario();
							
			if(!usuarioService.getEstado()) return "redirect:/Login?msg=Usuario inhabilitado, contacte con el administrador";
								
			if(tipoUsuario.equals("Administrador")) return "redirect:/Home/IntranetAdministrador";						
									
			return "redirect:/Home/IntranetUsuario";

			
					
		} catch (Exception e) {
			e.printStackTrace();
			return "Error";
		}		
	}
	
	@RequestMapping(value = "/CerrarSesion", method = RequestMethod.GET)
	public String CerrarSesion(HttpSession session) {	   
		
		session.removeAttribute("objUsuario");

	    return "redirect:/Login";
	}
}
