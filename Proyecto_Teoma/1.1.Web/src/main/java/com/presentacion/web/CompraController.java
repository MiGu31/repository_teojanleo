package com.presentacion.web;

import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.presentacion.pojo.entCompra;
import com.presentacion.pojo.entDetalleProductoCompra;
import com.presentacion.pojo.entDetalleProductoPedido;
import com.presentacion.pojo.entLinea;
import com.presentacion.pojo.entPedido;
import com.presentacion.pojo.entProducto;
import com.presentacion.pojo.entPromocion;
import com.presentacion.pojo.entProveedor;
import com.presentacion.pojo.entUsuario;

@Controller
public class CompraController {
	
	ResourceBundle resourceBundle = ResourceBundle.getBundle("route");	

	
	public ArrayList<entProducto> llenarListaProductos(String URL, RestTemplate rest) {
		ArrayList<entProducto> listaProductos = new ArrayList<entProducto>();
		ArrayList<entProducto> listaServiceProductos = rest.getForObject(URL, listaProductos.getClass()); 

		return listaServiceProductos;
	}
	
	
	public ArrayList<entLinea> llenarListaLineas(String URL, RestTemplate rest) {
		ArrayList<entLinea> listaLineas = new ArrayList<entLinea>();
		ArrayList<entLinea> listaServiceLineas = rest.getForObject(URL, listaLineas.getClass()); 

		return listaServiceLineas;
	}
	
	public ArrayList<entProveedor> llenarListaProveedores(String URL, RestTemplate rest) {
		ArrayList<entProveedor> listaProveedores = new ArrayList<entProveedor>();
		ArrayList<entProveedor> lsitaServiceProveedores = rest.getForObject(URL, listaProveedores.getClass()); 

		return lsitaServiceProveedores;
	}
	
	
	@RequestMapping(value = "/CORECompra/Insertar", method = RequestMethod.GET)
	public ModelAndView InsertarCompraGet() {
	
		try {
			RestTemplate rest = new RestTemplate();		
			
			String URListaProducto = resourceBundle.getString("URLService")+"Producto/ListarProductos";
			String URListaLinea = resourceBundle.getString("URLService")+"Linea/ListarLineas";	
			String URListaProveedores = resourceBundle.getString("URLService")+"Proveedor/ListarProveedores";	

			
			ModelAndView m = new ModelAndView("/CORECompra/Insertar");
			m.addObject("listaProductos",llenarListaProductos(URListaProducto,rest));													
			m.addObject("listaLineas",llenarListaLineas(URListaLinea,rest));	
			m.addObject("listaProveedores",llenarListaLineas(URListaProveedores,rest));	
			
			return m;		
			
		}
		catch (Exception e) {
			return new ModelAndView("frmError","error",e.getMessage());
		}
	}
	
	@RequestMapping(value = "/InsertarCompra", method = RequestMethod.POST)
	public ModelAndView InsertarCompraPOST(@RequestBody String json, Model model, HttpSession session)throws Exception {
	
		try {
			entUsuario objUsuarioSesion = (entUsuario) session.getAttribute("objUsuario");
			
			RestTemplate rest = new RestTemplate();
			
			ObjectMapper om = new ObjectMapper();			
			JsonNode jsonNode = om.readTree(json) ; 		
			
			ArrayList<entDetalleProductoCompra> lstDetalle =  om.convertValue(jsonNode.get("lstDetalle"),
					new TypeReference<ArrayList<entDetalleProductoCompra>>() {});	
			
			
			entProveedor objProveedor =  om.convertValue(jsonNode.get("objProveedor"),new entProveedor().getClass());	
						
			entCompra objCompra = new entCompra();
			entUsuario objUsuario = new entUsuario();
			
			objUsuario.setIdUsuario(objUsuarioSesion.getIdUsuario());
			objCompra.setObjUsuario(objUsuario);
			objCompra.setListaDetalle(lstDetalle);
			objCompra.setObjProveedor(objProveedor);
			
			String url = resourceBundle.getString("URLService")+"Compra/InsertarCompra";
			ResponseEntity<entCompra> result = rest.postForEntity(url, objCompra, entCompra.class);	
			
			ModelAndView m = new ModelAndView("/CORECompra/Insertar");				
					
			return m;		
			
		}
		catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("frmError","error",e.getMessage());
		}
	}
	
	
	@RequestMapping(value = "/CORECompra/Lista", method = RequestMethod.GET)
	public ModelAndView ListaCompras(Locale locale, Model model)throws Exception {
	
		try {
			
			ObjectMapper mapper = new ObjectMapper();		

			String URLCompras = resourceBundle.getString("URLService")+"Compra/ListarCompras";			
			ArrayList<entCompra> listaCompras = mapper.readValue(new URL(URLCompras),mapper.getTypeFactory().constructCollectionType(ArrayList.class, entCompra.class));							
												
			String URLDetalleCompra = resourceBundle.getString("URLService")+"DetalleProductoCompra/Listar";			
			ArrayList<entDetalleProductoCompra> listaDetalleProCompras = mapper.readValue(new URL(URLDetalleCompra),mapper.getTypeFactory().constructCollectionType(ArrayList.class, entDetalleProductoCompra.class)); 								
			
			for(entCompra co : listaCompras)
			{	
				ArrayList<entDetalleProductoCompra> listaDetalleComprasTemporal = new ArrayList<entDetalleProductoCompra>();
				for(entDetalleProductoCompra edp : listaDetalleProCompras)
				{
					if(co.getIdCompra()==edp.getObjCompra().getIdCompra()) {
						listaDetalleComprasTemporal.add(edp);
					}
				}
				co.setListaDetalle(listaDetalleComprasTemporal);
			}	
			
			
			ModelAndView m = new ModelAndView("CORECompra/Lista");
			m.addObject("listaCompras",listaCompras);
					
			return m;		
			
		}
		catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("frmError","error",e.getMessage());
		}
	}


}
