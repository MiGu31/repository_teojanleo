package com.presentacion.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class DescriptorServicioController {
	
	@RequestMapping(value = "/DescriptorServicios", method = RequestMethod.GET)
	public ModelAndView MostrarDescriptor() {		
		try {
			ModelAndView m = new  ModelAndView("/DescriptorServicios");
			
			Map<String, String> descripcionServicios = new HashMap<String, String>();
			
			descripcionServicios.put("Titulo: Verificaracceso", "Direccion de URL: Usuario/VerificarAcceso<br> Tipo de Solicitud: GET<br> Parametro de entrada: String Username, String Password<br> Parametro de salida: entUsuario");
			descripcionServicios.put("Titulo: CambioEstadoUsuario", "Direccion de URL: /Usuario/CambiarEstado<br> Tipo de Solicitud: GET<br> Parametro de entrada: int idUsuario<br> Parametro de salida: Boolean");
			descripcionServicios.put("Titulo: ListarReclamos ", "Direccion de URL: Reclamo/ListarReclamos <br> Tipo de Solicitud: GET<br> Parametro de entrada: - <br> Parametro de salida: ArrayList<entReclamo>");
			descripcionServicios.put("Titulo: InsertarReclamo", "Direccion de URL: Reclamo/InsertarReclamo<br> Tipo de Solicitud: POST<br> Parametro de entrada: entReclamo<br> Parametro de salida: ResponseEntity");
			descripcionServicios.put("Titulo: CambioDeEstadoReclamo ", "Direccion de URL: Reclamo/CambiarEstado<br> Tipo de Solicitud: GET<br> Parametro de entrada: int idReclamo<br> Parametro de salida: Boolean");
			descripcionServicios.put("Titulo: ListarPromociones ", "Direccion de URL: Promocion/ListarPromociones<br> Tipo de Solicitud: GET<br> Parametro de entrada: -<br> Parametro de salida: ArrayList<entPromocion>");
			descripcionServicios.put("Titulo: EliminarPromocion ", "Direccion de URL: Promocion/Eliminar<br> Tipo de Solicitud: GET<br> Parametro de entrada: int idPromocion<br> Parametro de salida: Boolean");
			descripcionServicios.put("Titulo: ListarProductos", "Direccion de URL: Producto/ListarProductos<br> Tipo de Solicitud: GET<br> Parametro de entrada: -<br> Parametro de salida: ArrayList<entProducto>");
			descripcionServicios.put("Titulo: InsertarProducto", "Direccion de URL: Producto/InsertarProducto<br> Tipo de Solicitud: POST<br> Parametro de entrada: entProducto<br> Parametro de salida: ResponseEntity");
			descripcionServicios.put("Titulo: EditarProducto", "Direccion de URL: Producto/EditarProducto<br> Tipo de Solicitud: POST<br> Parametro de entrada: entProducto<br> Parametro de salida: ResponseEntity");
			descripcionServicios.put("Titulo: EliminarProducto", "Direccion de URL: Producto/Eliminar<br> Tipo de Solicitud: GET<br> Parametro de entrada: int idProducto<br> Parametro de salida: Boolean");
			descripcionServicios.put("Titulo: ListarPersonas", "Direccion de URL: Persona/ListarPersonas<br> Tipo de Solicitud: GET<br> Parametro de entrada: -<br> Parametro de salida: ArrayList<entPersona>");
			descripcionServicios.put("Titulo: InsertarPersona", "Direccion de URL: Persona/InsertarPersona<br> Tipo de Solicitud: POST<br> Parametro de entrada: entPersona<br> Parametro de salida: ResponseEntity");
			descripcionServicios.put("Titulo: InsertarCliente", "Direccion de URL: Persona/InsertarCliente<br> Tipo de Solicitud: POST<br> Parametro de entrada: entPersona<br> Parametro de salida: ResponseEntity");
			descripcionServicios.put("Titulo: EditarCliente", "Direccion de URL: Persona/EditarCliente<br> Tipo de Solicitud: POST<br> Parametro de entrada: entPersona<br> Parametro de salida: ResponseEntity");
			descripcionServicios.put("Titulo: ListarClientes", "Direccion de URL: Persona/ListarClientes<br> Tipo de Solicitud: GET<br> Parametro de entrada: -<br> Parametro de salida: ArrayList<entPersona>");
			descripcionServicios.put("Titulo: InsertarPedido", "Direccion de URL: Pedido/InsertarPedido<br> Tipo de Solicitud: POST<br> Parametro de entrada: entPedido<br> Parametro de salida: ResponseEntity");
			descripcionServicios.put("Titulo: ListarPedidos", "Direccion de URL: Pedido/ListarPedidos<br> Tipo de Solicitud: GET<br> Parametro de entrada: int idUsuario<br> Parametro de salida: ArrayList<entPedido>");
			descripcionServicios.put("Titulo: ListarPedidosGenerales", "Direccion de URL: Pedido/ListarPedidosGenerales<br> Tipo de Solicitud: GET<br> Parametro de entrada: -<br> Parametro de salida: ArrayList<entPedido>");
			descripcionServicios.put("Titulo: InsertarLinea", "Direccion de URL: /Lineas/InsertarLineass<br> Tipo de Solicitud: GET<br> Parametro de entrada: String Nombre, String Imagen<br> Parametro de salida: Boolean");
			descripcionServicios.put("Titulo: ModificarLinea ", "Direccion de URL: /Lineas/ModificarLineas<br> Tipo de Solicitud: GET<br> Parametro de entrada: int idLinea, String Nombre, String Imagen<br> Parametro de salida: Boolean");
			descripcionServicios.put("Titulo: EliminarLineas", "Direccion de URL: /Lineas/EliminarLineasPro<br> Tipo de Solicitud: GET<br> Parametro de entrada: int idLinea<br> Parametro de salida: Boolean");	
			descripcionServicios.put("Titulo: ListarDetalleProductoPedido", "Direccion de URL: DetalleProductoPedidoController/Listar<br> Tipo de Solicitud: GET<br> Parametro de entrada: -<br> Parametro de salida: ArrayList<entDetalleProductoPedido>");
			descripcionServicios.put("Titulo: ListarDetalleProductoCompra", "Direccion de URL: DetalleProductoPedidoController/Listar<br> Tipo de Solicitud: GET<br> Parametro de entrada: -<br> Parametro de salida: ArrayList<entDetalleProductoCompra>");
			descripcionServicios.put("Titulo: InsertarCompra", "Direccion de URL: Compra/InsertarCompra<br> Tipo de Solicitud: POST<br> Parametro de entrada: entCompra<br> Parametro de salida: ResponseEntity");
			descripcionServicios.put("Titulo: ListarCompras", "Direccion de URL: Compra/ListarCompra<br> Tipo de Solicitud: GET<br> Parametro de entrada: -<br> Parametro de salida: ArrayList<entCompra>");
		
			
			m.addObject("listaDescripcion", descripcionServicios);
									
			
			return m;
			
		} catch (Exception e) {
			return new ModelAndView("frmError", "error", e.getMessage());
		}	
	}

}
