package com.presentacion.web;

import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.presentacion.pojo.entPedido;
import com.presentacion.pojo.entPersona;
import com.presentacion.pojo.entReclamo;
import com.presentacion.pojo.entUsuario;
import com.presentacion.util.CustomException;

@Controller
public class ReclamoController {
	
	ResourceBundle resourceBundle = ResourceBundle.getBundle("route");	
	
	@RequestMapping(value = "/CRUDReclamos/Lista", method = RequestMethod.GET)
    public ModelAndView ListarReclamos(HttpServletRequest req,HttpServletResponse response) throws Exception {
		
		try {
			
			RestTemplate rest = new RestTemplate();			

			if(req.getParameter("idReclamo")!=null) {
				//int idCambio = Integer.parseInt(req.getParameter("idUsuario"));
				String idString = req.getParameter("idReclamo");						
				int idInt = new Integer(idString.substring(0, idString.indexOf(" "))); 
				
				String URLi = resourceBundle.getString("URLService")+"Reclamo/CambiarEstado?idReclamo="+idInt;
				Boolean cambio = rest.getForObject(URLi, Boolean.class);

			}	
			
			String URL = resourceBundle.getString("URLService")+"Reclamo/ListarReclamos";
			ArrayList<entReclamo> listaReclamos = new ArrayList<entReclamo>();		
							
			ArrayList<entReclamo> listaService = rest.getForObject(URL, listaReclamos.getClass()); 		
			
			ModelAndView m = new ModelAndView("/CRUDReclamos/Lista");
			m.addObject("listaReclamos",listaService);
					
			return m;				
			
		}
		catch (Exception e) {
			return new ModelAndView("frmError","error",e.getMessage());
		}
         
    }
	
	@RequestMapping(value = "/CRUDReclamos/Insertar", method = RequestMethod.GET)
	public ModelAndView InsertarReclamoGET() {		
		try {
			return new ModelAndView("/CRUDReclamos/Insertar", "command", new entReclamo());
		} catch (Exception e) {
			return new ModelAndView("frmError", "error", e.getMessage());
		}	
	}
	
	@RequestMapping(value = "/CRUDReclamos/Insertar", method = RequestMethod.POST)
	public String InsertarReclamoPOST(@ModelAttribute("SpringWeb") entReclamo r, HttpSession session) {		
		try {
			entUsuario objUsuarioSesion = (entUsuario) session.getAttribute("objUsuario");

			
			entUsuario objUsuario = new entUsuario();
			objUsuario.setIdUsuario(objUsuarioSesion.getIdUsuario());
			r.setObjUsuario(objUsuario);
			r.setObjUsuario(objUsuarioSesion);
			RestTemplate rest = new RestTemplate();							

			//String url = resourceBundle.getString("URLService")+"Reclamo/InsertarReclamo";
			String url = resourceBundle.getString("URLService")+"Reclamo/InsertarReclamo";
			ResponseEntity<entReclamo> result = rest.postForEntity(url, r, entReclamo.class);	
			
			if(result!=null){
				return "redirect:/Home/IntranetUsuario";
			}
			else
			{
				return "redirect:/Home/IntranetUsuario";
			}			
			
		
			
		} catch (Exception e) {
			e.printStackTrace();
			return "Error";
		}	
	}

}
