package com.presentacion.web;

import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.presentacion.pojo.entPersona;
import com.presentacion.pojo.entUsuario;

@Controller
public class PersonaController {
	
	ResourceBundle resourceBundle = ResourceBundle.getBundle("route");	

	
	@RequestMapping(value = "/CRUDUsuario/Insertar", method = RequestMethod.POST)
	public String GuardarNuevousuario(@ModelAttribute("SpringWeb") entPersona p) {		
		try {
			RestTemplate rest = new RestTemplate();			
			
			if(p.getObjUsuario().getUsername() == "" || p.getObjUsuario().getPassword() =="")
			{
				return "redirect:/Login?msg= No se inserto nuevo usuario";
			}
			
			else {
				
				//String url = resourceBundle.getString("URLService")+"Persona/InsertarCliente";
				String url = resourceBundle.getString("URLService")+"Persona/InsertarCliente";
				ResponseEntity<entPersona> result = rest.postForEntity(url, p, entPersona.class);	
				
				String number = "asdasd";
				if(result!=null)			{
					return "redirect:/Login?msg= se inserto nuevo usuario";
				}
				else
				{
					return "redirect:/Login?msg= No se inserto nuevo usuario";
				}			
			}
		
			
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/Login?msg= No se inserto nuevo usuario, contacte con el administrador.";
		}	
	}	
	
	@RequestMapping(value = "/CRUDUsuario/Insertar", method = RequestMethod.GET)
	public ModelAndView NuevoUsuario() {		
		try {
			return new ModelAndView("/CRUDUsuario/Insertar", "command", new entPersona());
		} catch (Exception e) {
			return new ModelAndView("frmError", "error", e.getMessage());
		}	
	}	
	
	@RequestMapping(value = "/CRUDUsuario/Lista", method = RequestMethod.GET)
    public ModelAndView ListarClientes(HttpServletRequest req,HttpServletResponse response) throws Exception {
		
		try {			
			RestTemplate rest = new RestTemplate();			

			if(req.getParameter("idUsuario")!=null) {
				//int idCambio = Integer.parseInt(req.getParameter("idUsuario"));
				String idString = req.getParameter("idUsuario");						
				int idInt = new Integer(idString.substring(0, idString.indexOf(" "))); 
				
				String URLi = resourceBundle.getString("URLService")+"Usuario/CambiarEstado?idUsuario="+idInt;
				Boolean cambio = rest.getForObject(URLi, Boolean.class);

			}			
					
			//String URL = resourceBundle.getString("URLService")+"Persona/ListarClientes";
			String URL = resourceBundle.getString("URLService")+"Persona/ListarClientes";

			ArrayList<entPersona> listaClientes = new ArrayList<entPersona>();		
							
			ArrayList<entPersona> listaService = rest.getForObject(URL, listaClientes.getClass()); 		
			
			ModelAndView m = new ModelAndView("/CRUDUsuario/Lista");
			m.addObject("listaClientes",listaService);
					
			return m;				
			
		}
		catch (Exception e) {
			return new ModelAndView("frmError","error",e.getMessage());
		}
    }
	
	@RequestMapping(value = "/CRUDUsuario/EditarAdmi", method = RequestMethod.GET)
	public ModelAndView EditarUsuarioAdmi() {		
		try {
			return new ModelAndView("/CRUDUsuario/EditarAdmi", "command", new entPersona());
		} catch (Exception e) {
			return new ModelAndView("frmError", "error", e.getMessage());
		}	
	}	
	
	@RequestMapping(value = "/CRUDUsuario/EditarUser", method = RequestMethod.GET)
	public ModelAndView EditarUsuarioUser() {		
		try {
			return new ModelAndView("/CRUDUsuario/EditarUser", "command", new entPersona());
		} catch (Exception e) {
			return new ModelAndView("frmError", "error", e.getMessage());
		}	
	}	
	
	@RequestMapping(value = "/CRUDUsuario/EditarUser", method = RequestMethod.POST)
	public String EditarUser(@ModelAttribute("SpringWeb") entPersona p, HttpSession session) {		
		try {
			RestTemplate rest = new RestTemplate();			
			
			entUsuario objUsuario = new entUsuario();

			entUsuario objUsuarioSesion = (entUsuario) session.getAttribute("objUsuario");
			objUsuario.setIdUsuario(objUsuarioSesion.getIdUsuario());
			objUsuario.setEmail(p.getObjUsuario().getEmail());
			objUsuario.setPassword(p.getObjUsuario().getPassword());
			
			p.setObjUsuario(objUsuario);
			
			String url = resourceBundle.getString("URLService")+"Persona/EditarCliente";
			ResponseEntity<entPersona> result = rest.postForEntity(url, p, entPersona.class);	
			
			if(result!=null){
				return "redirect:/Home/IntranetUsuario";
			}
			else
			{
				return "redirect:/Home/IntranetUsuario";
			}			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:?msg= No se actualizo, contacte con el administrador.";
		}	
	}	
	
	
	@RequestMapping(value = "/CRUDUsuario/EditarAdmi", method = RequestMethod.POST)
	public String EditarAdmi(@ModelAttribute("SpringWeb") entPersona p, HttpSession session) {		
		try {
			RestTemplate rest = new RestTemplate();			
			
			
			entUsuario objUsuario = new entUsuario();

			entUsuario objUsuarioSesion = (entUsuario) session.getAttribute("objUsuario");
			objUsuario.setIdUsuario(objUsuarioSesion.getIdUsuario());
			objUsuario.setEmail(p.getObjUsuario().getEmail());
			objUsuario.setPassword(p.getObjUsuario().getPassword());
			
			p.setObjUsuario(objUsuario);
			
				
			String url = resourceBundle.getString("URLService")+"Persona/EditarCliente";
			ResponseEntity<entPersona> result = rest.postForEntity(url, p, entPersona.class);	
			
			if(result!=null)			{
				return "redirect:/Home/IntranetAdministrador";
			}
			else
			{
				return "redirect:/Home/IntranetAdministrador";
			}	
		
			
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/?msg= No se actualizo, contacte con el administrador.";
		}	
	}

}
