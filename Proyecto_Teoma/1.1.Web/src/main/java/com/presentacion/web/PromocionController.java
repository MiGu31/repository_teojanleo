package com.presentacion.web;

import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.presentacion.pojo.entPromocion;
import com.presentacion.pojo.entReclamo;
import com.presentacion.pojo.entUsuario;

@Controller
public class PromocionController {
	
	ResourceBundle resourceBundle = ResourceBundle.getBundle("route");	

	
	@RequestMapping(value = "/CRUDPromocion/Lista", method = RequestMethod.GET)
    public ModelAndView ListarPromociones(HttpServletRequest req,HttpServletResponse response) throws Exception {
		
		try {
			RestTemplate rest = new RestTemplate();		
			
			if(req.getParameter("idPromocion")!=null) {
				String idString = req.getParameter("idPromocion");						
				int idInt = new Integer(idString.substring(0, idString.indexOf(" "))); 
				
				String URLi = resourceBundle.getString("URLService")+"Promocion/Eliminar?idPromocion="+idInt;
				Boolean elimino = rest.getForObject(URLi, Boolean.class);

			}	

			
			String URL = resourceBundle.getString("URLService")+"Promocion/ListarPromociones";
			ArrayList<entPromocion> listaPromociones = new ArrayList<entPromocion>();		
							
			ArrayList<entPromocion> listaService = rest.getForObject(URL, listaPromociones.getClass()); 		
			
			ModelAndView m = new ModelAndView("/CRUDPromocion/Lista");
			m.addObject("listaPromociones",listaService);
					
			return m;				
			
		}
		catch (Exception e) {
			return new ModelAndView("frmError","error",e.getMessage());
		}
         
    }
	
	@RequestMapping(value = "/CRUDPromocion/Insertar", method = RequestMethod.GET)
	public ModelAndView InsertarPromocionGET() {		
		try {
			return new ModelAndView("/CRUDPromocion/Insertar", "command", new entPromocion());
		} catch (Exception e) {
			return new ModelAndView("frmError", "error", e.getMessage());
		}	
	}
	
	@RequestMapping(value = "/CRUDPromocion/Insertar", method = RequestMethod.POST)
	public String InsertarReclamoPOST(@ModelAttribute("SpringWeb") entPromocion p) {	
		
		try {		
	
			RestTemplate rest = new RestTemplate();							

			//String url = resourceBundle.getString("URLService")+"Reclamo/InsertarReclamo";
			String url = resourceBundle.getString("URLService")+"Promocion/InsertarPromocion";
			ResponseEntity<entPromocion> result = rest.postForEntity(url, p, entPromocion.class);	
			
			if(result!=null){
				return "redirect:/CRUDPromocion/Lista";
			}
			else
			{
				return "redirect:/Home/IntranetAdministrador";	
			}			
			
		} catch (Exception e) {			
			
			e.printStackTrace();
			return "redirect:/CRUDPromocion/Insertar?msg= Existe una promocion vigente, inhabilitela o espere su vencimiento.";
		}	
	}

}
