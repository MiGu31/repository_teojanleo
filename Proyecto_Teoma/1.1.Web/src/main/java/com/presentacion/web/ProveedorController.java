package com.presentacion.web;

import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.presentacion.pojo.entDetalleProductoPedido;
import com.presentacion.pojo.entLinea;
import com.presentacion.pojo.entProducto;
import com.presentacion.pojo.entProveedor;
import com.presentacion.pojo.entReclamo;
import com.presentacion.pojo.entUsuario;


@Controller
public class ProveedorController {
	
	ResourceBundle resourceBundle = ResourceBundle.getBundle("route");	
	
	@RequestMapping(value = "/CRUDProveedor/Lista", method = RequestMethod.GET)
	@ResponseBody
    public ModelAndView ListarProveedores(HttpServletRequest req,HttpServletResponse response) throws Exception {
		
		try {
			RestTemplate rest = new RestTemplate();		
				
			String URL = resourceBundle.getString("URLService")+"Proveedor/ListarProveedores";
			ArrayList<entProveedor> listaProveedores = new ArrayList<entProveedor>();	
			ArrayList<entProveedor> listaService = rest.getForObject(URL, listaProveedores.getClass()); 		
			
			ModelAndView m = new ModelAndView("/CRUDProveedor/Lista");
			m.addObject("listaProveedores",listaService);
					
			return m;				
			
		}
		catch (Exception e) {
			return new ModelAndView("frmError","error",e.getMessage());
		}
         
    }
	
	@RequestMapping(value = "/CRUDProveedor/ListaProductos", method = RequestMethod.GET)
    public ModelAndView ListarProductosPorProveedor(HttpServletRequest req,HttpServletResponse response, HttpSession session) throws Exception {
		
		try {
			RestTemplate rest = new RestTemplate();	
			RestTemplate restAdd = new RestTemplate();		
			
			String idString = req.getParameter("idProveedor");						
			int idInt = new Integer(idString.substring(0, idString.indexOf(" "))); 
			session.setAttribute("idProveedor", idInt);
			
			
			String URLproveedor = resourceBundle.getString("URLService")+"Proveedor/ListarProductos?idProveedor="+idInt;
			String URLproductos = resourceBundle.getString("URLService")+"Producto/ListarProductos";

			ArrayList<entProducto> listaProductos = new ArrayList<entProducto>();	
			ArrayList<entProducto> listaService = rest.getForObject(URLproveedor, listaProductos.getClass()); 	
			
			ArrayList<entProducto> listaProductosAdd = new ArrayList<entProducto>();	
			ArrayList<entProducto> listaServiceAdd = rest.getForObject(URLproductos, listaProductosAdd.getClass()); 
			
			
			ModelAndView m = new ModelAndView("/CRUDProveedor/ListaProductos", "command", new entProducto());
			m.addObject("listaProductos",listaService);
			m.addObject("listaProductosAdd",listaServiceAdd);
		
			return m;				
			
		}
		catch (Exception e) {
			return new ModelAndView("frmError","error",e.getMessage());
		}
         
    }
	
		
	@RequestMapping(value = "/CRUDProveedor/Insertar", method = RequestMethod.POST)
	public String InsertarProveedorPOST(@ModelAttribute("SpringWeb") entProveedor objProveedor) {		
		try {
			RestTemplate rest = new RestTemplate();			
			
			if(objProveedor.getNombre().equals(""))
			{
				return "redirect:/CRUDProveedor/Insertar";
			}
			
			else {
				
				String url = resourceBundle.getString("URLService")+"Proveedor/InsertarProveedor";
				ResponseEntity<entProveedor> result = rest.postForEntity(url, objProveedor, entProveedor.class);	
				
				if(result!=null)			{
					return "redirect:/CRUDProveedor/Lista";
				}
				else
				{
					return "redirect:/CRUDProveedor/Insertar";
				}			
			}
		
			
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/CRUDProveedor/Insertar?msg= No se inserto proveedor, contacte con el administrador.";
		}	
	}	
	
	@RequestMapping(value = "/CRUDProveedor/Insertar", method = RequestMethod.GET)
	public ModelAndView InsertarProveedorGET() {		
		try {			
			ModelAndView m = new ModelAndView("/CRUDProveedor/Insertar", "command", new entProveedor());
			return m;

		} catch (Exception e) {
			return new ModelAndView("frmError", "error", e.getMessage());
		}	
	}
		
	@RequestMapping(value = "/InsertarProductos", method = RequestMethod.POST)
	@ResponseBody
	public String InsertarProductosPOST(@RequestBody String json,Model model, HttpSession session) throws Exception{		
		try {
			RestTemplate rest = new RestTemplate();		
			
			entProveedor objProveedor = new entProveedor();
			
			objProveedor.setIdProveedor((Integer) session.getAttribute("idProveedor"));

			
			ObjectMapper om = new ObjectMapper();			
			JsonNode jsonNode = om.readTree(json) ; 		
			
			ArrayList<Integer> lstNumeros =  om.convertValue(jsonNode.get("lstProductos"),
					new TypeReference<ArrayList<Integer>>() {});	
			
			ArrayList<entProducto> listaProductos = new ArrayList<entProducto>();
			
			for(Integer entero : lstNumeros) {
				entProducto objProducto = new entProducto();
				objProducto.setIdProducto(entero);	
				listaProductos.add(objProducto);
			}
			
			objProveedor.setListaProductos(listaProductos);
			
			String url = resourceBundle.getString("URLService")+"DetalleProveedorProducto/Insertar";		
			

			ResponseEntity<entProveedor> result = rest.postForEntity(url, objProveedor, entProveedor.class);	
					
						
			if(result!=null)			{
				return "redirect:/CRUDProveedor/Lista";
			}
			else
			{
				return "redirect:/CRUDProveedor/Lista";
			}	
						
			
		} catch (Exception e) {
			return "error";
		}	
	}

}
