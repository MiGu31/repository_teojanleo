package com.presentacion.pojo;

import java.sql.Date;
import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)

public class entPedido {
	
	private int idPedido;
	private Date Fecha;
	private float CostoTotal;
	private entPromocion objPromocion;
	private entUsuario objUsuario;
	private ArrayList<entDetalleProductoPedido> ListaDetalle;
	
	public int getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}

	public Date getFecha() {
		return Fecha;
	}
	public void setFecha(Date fecha) {
		Fecha = fecha;
	}
	public float getCostoTotal() {
		return CostoTotal;
	}
	public void setCostoTotal(float costoTotal) {
		CostoTotal = costoTotal;
	}
	public entPromocion getObjPromocion() {
		return objPromocion;
	}
	public void setObjPromocion(entPromocion objPromocion) {
		this.objPromocion = objPromocion;
	}
	public entUsuario getObjUsuario() {
		return objUsuario;
	}
	public void setObjUsuario(entUsuario objUsuario) {
		this.objUsuario = objUsuario;
	}
	
	public ArrayList<entDetalleProductoPedido> getListaDetalle() {
		return ListaDetalle;
	}
	public void setListaDetalle(ArrayList<entDetalleProductoPedido> listaDetalle) {
		ListaDetalle = listaDetalle;
	}
		
}
