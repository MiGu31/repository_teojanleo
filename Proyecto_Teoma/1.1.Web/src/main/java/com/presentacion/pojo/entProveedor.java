package com.presentacion.pojo;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class entProveedor {
	
	private int idProveedor;
	private String Nombre;
	private int Celular;
	private String Direccion;
	private String Correo;

	private ArrayList<entProducto> listaProductos;

	public int getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public int getCelular() {
		return Celular;
	}
	public void setCelular(int celular) {
		Celular = celular;
	}
	public String getDireccion() {
		return Direccion;
	}
	public void setDireccion(String direccion) {
		Direccion = direccion;
	}
	
	public String getCorreo() {
		return Correo;
	}
	public void setCorreo(String correo) {
		Correo = correo;
	}
	
	public ArrayList<entProducto> getListaProductos() {
		return listaProductos;
	}
	public void setListaProductos(ArrayList<entProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}	

}
