package com.presentacion.pojo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)

public class entDetalleProductoCompra {
	
	private int idDetalleProductoCompra;
	private int Cantidad;
	private entProducto objProducto;
	private entCompra objCompra;
	public int getIdDetalleProductoCompra() {
		return idDetalleProductoCompra;
	}
	public void setIdDetalleProductoCompra(int idDetalleProductoCompra) {
		this.idDetalleProductoCompra = idDetalleProductoCompra;
	}
	public int getCantidad() {
		return Cantidad;
	}
	public void setCantidad(int cantidad) {
		Cantidad = cantidad;
	}
	public entProducto getObjProducto() {
		return objProducto;
	}
	public void setObjProducto(entProducto objProducto) {
		this.objProducto = objProducto;
	}
	public entCompra getObjCompra() {
		return objCompra;
	}
	public void setObjCompra(entCompra objCompra) {
		this.objCompra = objCompra;
	}
	
	

}
