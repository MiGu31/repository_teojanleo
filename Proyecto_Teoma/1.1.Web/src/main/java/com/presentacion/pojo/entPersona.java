package com.presentacion.pojo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)

public class entPersona {
	
	private int idPersona;
	private String Dni;
	private String Nombres;
	private String Apellidos;
	private String TelefonoCelular;
	private String TelefonoFijo;
	private String Direccion;
	private entUsuario objUsuario;
	public int getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}
	public String getDni() {
		return Dni;
	}
	public void setDni(String dni) {
		Dni = dni;
	}
	public String getNombres() {
		return Nombres;
	}
	public void setNombres(String nombres) {
		Nombres = nombres;
	}
	public String getApellidos() {
		return Apellidos;
	}
	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}
	public String getTelefonoCelular() {
		return TelefonoCelular;
	}
	public void setTelefonoCelular(String telefonoCelular) {
		TelefonoCelular = telefonoCelular;
	}
	public String getTelefonoFijo() {
		return TelefonoFijo;
	}
	public void setTelefonoFijo(String telefonoFijo) {
		TelefonoFijo = telefonoFijo;
	}
	public String getDireccion() {
		return Direccion;
	}
	public void setDireccion(String direccion) {
		Direccion = direccion;
	}
	public entUsuario getObjUsuario() {
		return objUsuario;
	}
	public void setObjUsuario(entUsuario objUsuario) {
		this.objUsuario = objUsuario;
	}
	
	

}
