package com.presentacion.pojo;

import java.sql.Date;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown=true)

public class entReclamo {
	
	private int idReclamo;
	private String Motivo;
	private String Descripcion;
	private Date Fecha;
	private boolean Estado;
	private entUsuario objUsuario;
	public int getIdReclamo() {
		return idReclamo;
	}
	public void setIdReclamo(int idReclamo) {
		this.idReclamo = idReclamo;
	}
	public String getMotivo() {
		return Motivo;
	}
	public void setMotivo(String motivo) {
		Motivo = motivo;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public Date getFecha() {
		return Fecha;
	}
	public void setFecha(Date fecha) {
		Fecha = fecha;
	}
	public entUsuario getObjUsuario() {
		return objUsuario;
	}
	public void setObjUsuario(entUsuario objUsuario) {
		this.objUsuario = objUsuario;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}

}
